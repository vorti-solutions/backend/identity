﻿using System.Net;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Text.Json;
using Asp.Versioning;
using AspNetCoreRateLimit;
using FluentValidation;
using FluentValidation.AspNetCore;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using Vorti.Authentication.API.Handlers;
using Vorti.Authentication.Application.Behaviours;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using Vorti.Authentication.Application.Contracts.Infrastructure.Notification;
using Vorti.Authentication.Application.Contracts.Infrastructure.Persistence;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Application.Features.Login.Commands;
using Vorti.Authentication.Application.Mappings;
using Vorti.Authentication.Domain.Common.Configs;
using Vorti.Authentication.Domain.Common.Utilities;
using Vorti.Authentication.Domain.Entities;
using Vorti.Authentication.Infrastructure.Identity;
using Vorti.Authentication.Infrastructure.Notification;
using Vorti.Authentication.Infrastructure.Persistence;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using Utility = Vorti.Authentication.Domain.Common.Utilities.Utility;

namespace Vorti.Authentication.API.Extensions
{
    public static class ServiceCollectionExtension
    {
        private static MongoDbConfig _mongoDbSettings;
        private static AuthConfig _authConfig;

        public static void RegisterMapster(this IServiceCollection services)
        {
            services.AddMapster();
            MapsterConfig.Configure();
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IAuthorizationHandler, RoleRequirementHandler>();
            services.AddScoped<IUserIdentityService, UserIdentityService>();
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<IResultResponse, ResultResponse>();
            services.AddScoped<IRepositoryManager, RepositoryManager>();
            services.AddSingleton(typeof(IRepository<>), typeof(Repository<>));
            services.AddSingleton(typeof(IRepositoryContext), typeof(RepositoryContext));
            services.AddTransient<IUtility, Utility>();
        }

        public static void RegisterRateLimiting(this IServiceCollection services)
        {
            var rateLimitRules = new List<RateLimitRule>
            {
                new()
                {
                    Endpoint = "*",
                    Limit = 100,
                    Period = "1s",
                },
            };

            services.Configure<IpRateLimitOptions>(opt =>
            {
                opt.GeneralRules = rateLimitRules;
                //opt.EndpointWhitelist = [""];
            });

            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            services.AddSingleton<IProcessingStrategy, AsyncKeyLockProcessingStrategy>();
        }

        public static void RegisterMediatR(this IServiceCollection services)
        {
            services.AddMediatR(cfg =>
                cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly())
            );
            services.AddTransient(
                typeof(IPipelineBehavior<,>),
                typeof(UnhandledExceptionBehaviour<,>)
            );
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
        }

        public static void RegisterValidators(this IServiceCollection services)
        {
            services.AddFluentValidationAutoValidation(x =>
                x.DisableDataAnnotationsValidation = true
            );
            services.AddValidatorsFromAssemblyContaining<LoginCommand>();
        }

        public static void RegisterFilters(this IServiceCollection services)
        {
            services.AddRouting(x => x.LowercaseUrls = true);
            services
                .AddControllers(x =>
                {
                    x.Filters.Add(new ProducesAttribute(MediaTypeNames.Application.Json));
                    x.Filters.Add(new ConsumesAttribute(MediaTypeNames.Application.Json));
                    x.Filters.Add(
                        new ProducesResponseTypeAttribute(
                            typeof(ErrorResponse),
                            (int)HttpStatusCode.BadRequest
                        )
                    );
                    x.Filters.Add(
                        new ProducesResponseTypeAttribute(
                            typeof(ErrorResponse),
                            (int)HttpStatusCode.UnprocessableEntity
                        )
                    );
                })
                .AddJsonOptions(x =>
                    x.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                );
        }

        public static void RegisterAppsettingConfigs(
            this IServiceCollection services,
            ConfigurationManager configuration
        )
        {
            _mongoDbSettings = configuration.GetSection(nameof(MongoDbConfig)).Get<MongoDbConfig>();
            _authConfig = configuration.GetSection(nameof(AuthConfig)).Get<AuthConfig>();
            services.Configure<MongoDbConfig>(configuration.GetSection(nameof(MongoDbConfig)));
            services.Configure<AuthConfig>(configuration.GetSection(nameof(AuthConfig)));
            services.Configure<EmailConfig>(configuration.GetSection(nameof(EmailConfig)));
            services.Configure<Google2FAConfig>(configuration.GetSection(nameof(Google2FAConfig)));
            services.Configure<AppConfig>(configuration.GetSection(nameof(AppConfig)));
        }

        public static void RegisterApiVersion(this IServiceCollection services)
        {
            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
                options.ApiVersionReader = new UrlSegmentApiVersionReader();
            });
        }

        public static void RegisterSwaggerGen(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition(
                    JwtBearerDefaults.AuthenticationScheme,
                    new OpenApiSecurityScheme()
                    {
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.Http,
                        Name = HeaderNames.Authorization,
                        BearerFormat = Constants.Util.BEARER_FORMAT,
                        Scheme = JwtBearerDefaults.AuthenticationScheme,
                        Description = Constants.Util.BEARER_DESCRIPTION,
                    }
                );
            });
        }

        public static void RegisterAuthentication(this IServiceCollection services)
        {
            services
                .AddAuthentication(opt =>
                {
                    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(
                    JwtBearerDefaults.AuthenticationScheme,
                    opt =>
                    {
                        opt.SaveToken = true;
                        opt.RequireHttpsMetadata = false;
                        opt.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidIssuer = _authConfig.ValidIssuer,
                            ValidAudience = _authConfig.ValidAudience,
                            IssuerSigningKey = new SymmetricSecurityKey(
                                Encoding.UTF8.GetBytes(_authConfig.Secret)
                            ),
                        };
                    }
                );
        }

        public static void RegisterAuthorization(this IServiceCollection services)
        {
            services
                .AddAuthorizationBuilder()
                .AddPolicy(
                    Constants.SysRole.ADMIN,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.AddRequirements(new RoleRequirement(Constants.SysRole.ADMIN));
                    }
                )
                .AddPolicy(
                    Constants.SysRole.DRIVER,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.AddRequirements(new RoleRequirement(Constants.SysRole.DRIVER));
                    }
                )
                .AddPolicy(
                    Constants.SysRole.VENDOR,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.AddRequirements(new RoleRequirement(Constants.SysRole.VENDOR));
                    }
                )
                .AddPolicy(
                    Constants.SysRole.CUSTOMER,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.AddRequirements(new RoleRequirement(Constants.SysRole.CUSTOMER));
                    }
                )
                .AddPolicy(
                    Constants.SysRole.SUPER_ADMIN,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.AddRequirements(new RoleRequirement(Constants.SysRole.SUPER_ADMIN));
                    }
                )
                .AddPolicy(
                    Constants.SysRole.DELIVERY_AGENT,
                    policy =>
                    {
                        policy.RequireAuthenticatedUser();
                        policy.AddRequirements(
                            new RoleRequirement(Constants.SysRole.DELIVERY_AGENT)
                        );
                    }
                );
        }

        public static void RegisterRepositoryContext(this IServiceCollection services)
        {
            services
                .AddIdentity<ApplicationUser, ApplicationRole>(opt =>
                {
                    opt.SignIn.RequireConfirmedEmail = true;
                    opt.User.RequireUniqueEmail = true;
                    opt.Password.RequiredLength = 8;
                    opt.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromHours(24);
                    opt.Lockout.MaxFailedAccessAttempts = 5;
                })
                .AddMongoDbStores<ApplicationUser, ApplicationRole, Guid>(
                    _mongoDbSettings.ConnectionString,
                    _mongoDbSettings.Database
                )
                .AddDefaultTokenProviders();

            //initialize database
            var mongoClient = new MongoClient(_mongoDbSettings.ConnectionString);
            var mongoDbInitializer = new MongoDbInitializer(
                mongoClient,
                _mongoDbSettings.Database,
                _mongoDbSettings
            );
            mongoDbInitializer.InitializeDatabase();
        }

        public static void RegisterGrpc(this IServiceCollection services)
        {
            services.AddGrpc(opts =>
            {
                opts.EnableDetailedErrors = true;
            });

            services.AddGrpcReflection();
        }
    }
}
