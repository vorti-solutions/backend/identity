﻿using Microsoft.AspNetCore.Authorization;

namespace Vorti.Authentication.API.Handlers
{
    public class RoleRequirement(string role) : IAuthorizationRequirement
    {
        public string Role { get; set; } = role;
    }
}