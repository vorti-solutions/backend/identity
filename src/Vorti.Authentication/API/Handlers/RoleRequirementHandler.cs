﻿using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Vorti.Authentication.API.Handlers
{
    public class RoleRequirementHandler : AuthorizationHandler<RoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleRequirement requirement)
        {
            IEnumerable<IAuthorizationRequirement> requirements = context.Requirements;

            if (!context.User.HasClaim(c => c.Type == ClaimTypes.Role))
            {
                context.Fail(new AuthorizationFailureReason(this, "User token has no role."));
                return Task.CompletedTask;
            }

            string role = context.User.FindFirstValue(ClaimTypes.Role);
            string[] roles = role.Split(',', StringSplitOptions.RemoveEmptyEntries);
            //string expectedRole = requirement.Role;

            string[] requiredRoles = requirements.Where(i => i.GetType() == typeof(RoleRequirement)).Select(i => ((RoleRequirement)i).Role).ToArray();
            bool isMatch = requiredRoles.Any(x => roles.Any(y => x == y));

            if (!isMatch)
            {
                context.Fail(new AuthorizationFailureReason(this, "User token does not have the required role."));
                return Task.CompletedTask;
            }

            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}