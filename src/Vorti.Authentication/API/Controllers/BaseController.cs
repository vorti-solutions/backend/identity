﻿using Microsoft.AspNetCore.Mvc;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.API.Controllers
{
    public class BaseController : ControllerBase
    {
        protected ActionResult ResolveActionResult<T>(T response, string actionName = default, Guid? id = default) where T : BaseResponse
        {
            return response.StatusCode switch
            {
                StatusCodes.Status200OK => Ok(response),
                StatusCodes.Status201Created => string.IsNullOrEmpty(actionName) || !id.HasValue
                    ? StatusCode(StatusCodes.Status201Created, response)
                    : CreatedAtAction(actionName, new { id }, response),
                StatusCodes.Status204NoContent => NoContent(),
                StatusCodes.Status400BadRequest => BadRequest(response),
                StatusCodes.Status404NotFound => NotFound(response),
                StatusCodes.Status401Unauthorized => Unauthorized(response),
                StatusCodes.Status500InternalServerError => StatusCode(StatusCodes.Status500InternalServerError, response),
                _ => UnprocessableEntity(response),
            };
        }
    }
}