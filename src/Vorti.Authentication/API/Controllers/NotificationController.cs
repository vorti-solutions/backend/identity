﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using System.Net;
using Vorti.Authentication.Application.Features.Notifications.Email.Commands;
using Vorti.Authentication.Application.Features.Notifications.Otp.Commands;
using Vorti.Authentication.Domain.Common.Utilities;
using Microsoft.AspNetCore.Authorization;
using Asp.Versioning;
using Vorti.Authentication.Application.Contracts.Utility;

namespace Vorti.Authentication.API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route(Routes.Base)]
    public class NotificationController(IMediator mediator, IUtility utility) : BaseController
    {
        private readonly IMediator _mediator = mediator;
        private readonly IUtility _utility = utility;

        [AllowAnonymous, HttpPost(Routes.SEND_OTP)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SendOtp([FromBody]RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<OtpCommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [AllowAnonymous, HttpPost(Routes.SEND_EMAIL)]
        [Consumes(contentType: Constants.Util.FORM_DATA)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SendEmail([FromForm]EmailCommand command, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }
    }
}