﻿using Asp.Versioning;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Net;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Application.Features.ActivateUser.Commands;
using Vorti.Authentication.Application.Features.ChangeEmail;
using Vorti.Authentication.Application.Features.ChangePassword;
using Vorti.Authentication.Application.Features.ChangePhoneNumber;
using Vorti.Authentication.Application.Features.DeactivateUser.Commands;
using Vorti.Authentication.Application.Features.Login.Commands;
using Vorti.Authentication.Application.Features.ResetPassword;
using Vorti.Authentication.Application.Features.Set2FA.Commands;
using Vorti.Authentication.Application.Features.Set2FA.Queries;
using Vorti.Authentication.Application.Features.Users.UserCreation.Commands;
using Vorti.Authentication.Application.Features.Users.UserOnboarding.Commands;
using Vorti.Authentication.Domain.Common.Utilities;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route(Routes.Base)]
    public class AuthController(IMediator mediator, IUtility utility) : BaseController
    {
        private readonly IMediator _mediator = mediator;
        private readonly IUtility _utility = utility;

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN), HttpPost(Routes.ACTIVATE_USER)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ActivateUser([BindRequired, FromQuery]Guid userId, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(new ActivateUserCommand(userId), cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN), HttpPost(Routes.DEACTIVATE_USER)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeactivateUser([BindRequired, FromQuery]Guid userId, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(new DeactivateUserCommand(userId), cancellationToken));
        }

        [AllowAnonymous, HttpPost(Routes.LOGIN)]
        [ProducesResponseType(typeof(ServiceResponse<LoginResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Login([FromBody]RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<LoginCommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN)]
        [Authorize(Policy = Constants.SysRole.ADMIN)]
        [Authorize(Policy = Constants.SysRole.DRIVER)]
        [Authorize(Policy = Constants.SysRole.VENDOR)]
        [Authorize(Policy = Constants.SysRole.CUSTOMER)]
        [Authorize(Policy = Constants.SysRole.DELIVERY_AGENT)]
        [HttpGet(Routes.MULTI_FACTOR_STATUS)]
        [ProducesResponseType(typeof(ServiceResponse<UserMultiFactorResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetMultiFactorStatus(CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(new GetMultiFactorStatusQuery(), cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN)]
        [Authorize(Policy = Constants.SysRole.ADMIN)]
        [Authorize(Policy = Constants.SysRole.DRIVER)]
        [Authorize(Policy = Constants.SysRole.VENDOR)]
        [Authorize(Policy = Constants.SysRole.CUSTOMER)]
        [Authorize(Policy = Constants.SysRole.DELIVERY_AGENT)]
        [HttpPost(Routes.SET_MULTI_FACTOR)]
        [ProducesResponseType(typeof(ServiceResponse<UserTwoFactorResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetUp2FA([FromBody] SetMultiFactorCommand command, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [AllowAnonymous, HttpPost(Routes.LOGIN_MULTI_FACTOR)]
        [ProducesResponseType(typeof(ServiceResponse<LoginResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Login2FA([FromBody] RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<Login2FACommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN)]
        [Authorize(Policy = Constants.SysRole.ADMIN)]
        [Authorize(Policy = Constants.SysRole.DRIVER)]
        [Authorize(Policy = Constants.SysRole.VENDOR)]
        [Authorize(Policy = Constants.SysRole.CUSTOMER)]
        [Authorize(Policy = Constants.SysRole.DELIVERY_AGENT)]
        [HttpPut(Routes.CHANGE_PASSWORD)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> ChangePassword([FromBody] RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<ChangePasswordCommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [AllowAnonymous, HttpPost(Routes.INITIATE_RESET_PASSWORD)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> InitiateResetPassword([BindRequired, FromQuery]string email, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(new InitiateResetPasswordCommand(email), cancellationToken));
        }

        [AllowAnonymous, HttpPost(Routes.RESET_PASSWORD)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CompleteResetPassword([FromBody]RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<CompleteResetPasswordCommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN)]
        [Authorize(Policy = Constants.SysRole.ADMIN)]
        [Authorize(Policy = Constants.SysRole.DRIVER)]
        [Authorize(Policy = Constants.SysRole.VENDOR)]
        [Authorize(Policy = Constants.SysRole.CUSTOMER)]
        [Authorize(Policy = Constants.SysRole.DELIVERY_AGENT)]
        [HttpPut(Routes.CHANGE_PHONE_NUMBER)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdatePhoneNumber([BindRequired, FromQuery]string phoneNumber, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(new ChangePhoneNumberCommand(phoneNumber), cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN)]
        [Authorize(Policy = Constants.SysRole.ADMIN)]
        [Authorize(Policy = Constants.SysRole.DRIVER)]
        [Authorize(Policy = Constants.SysRole.VENDOR)]
        [Authorize(Policy = Constants.SysRole.CUSTOMER)]
        [Authorize(Policy = Constants.SysRole.DELIVERY_AGENT)]
        [HttpPost(Routes.INITIATE_EMAIL_CHANGE)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> InitiateChangeEmail([BindRequired, FromQuery]string email, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(new InitiateChangeEmailCommand(email), cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN)]
        [Authorize(Policy = Constants.SysRole.ADMIN)]
        [Authorize(Policy = Constants.SysRole.DRIVER)]
        [Authorize(Policy = Constants.SysRole.VENDOR)]
        [Authorize(Policy = Constants.SysRole.CUSTOMER)]
        [Authorize(Policy = Constants.SysRole.DELIVERY_AGENT)]
        [HttpPut(Routes.EMAIL_CHANGE)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CompleteChangeEmail([FromBody]RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<CompleteChangeEmailCommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [Authorize(Policy = Constants.SysRole.SUPER_ADMIN)]
        [Authorize(Policy = Constants.SysRole.ADMIN)]
        [HttpPost(Routes.ONBOARD_USER)]
        [ProducesResponseType(typeof(ServiceResponse<UserIdResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> OnboardVendor([FromBody]UserOnboardingCommand command, CancellationToken cancellationToken = default)
        {
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [AllowAnonymous, HttpPost(Routes.INITIATE_SIGNUP)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> InitiateVendorSignup([FromBody]RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<InitiateUserSignupCommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }

        [AllowAnonymous, HttpPost(Routes.COMPLETE_SIGNUP)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CompleteVendorSignup([FromBody]RequestPayload request, CancellationToken cancellationToken = default)
        {
            var command = _utility.DecryptRequest<CompleteUserSignupCommand>(request);
            return ResolveActionResult(await _mediator.Send(command, cancellationToken));
        }
    }
}