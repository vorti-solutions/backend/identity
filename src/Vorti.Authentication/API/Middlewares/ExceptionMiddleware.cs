﻿using System.Text.Json;
using Vorti.Authentication.Domain.Common.Utilities;
using Microsoft.AspNetCore.Diagnostics;
using FluentValidation;
using Vorti.Authentication.Application.Contracts.Utility;
using Mapster;
using System.Net.Mime;

namespace Vorti.Authentication.API.Middlewares
{
    public static class ExceptionMiddleware
    {
        private static string response;

        public static void UseAppExceptionHandler(this IApplicationBuilder app, ILogger logger, IResultResponse resultResponse)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.ContentType = MediaTypeNames.Application.Json;
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        logger.LogError($"Something went wrong: {contextFeature.Error}");
                        if (contextFeature.Error is ValidationException validationException)
                        {
                            response = WriteValidationException(validationException, context, resultResponse);
                        }
                        else if (contextFeature.Error is CompileException compileException)
                        {
                            response = WriteMappingException(compileException, context, resultResponse);
                        }
                        else
                        {
                            response = WriteGenericException(contextFeature.Error, context, resultResponse);
                        }
                        await context.Response.WriteAsync(response);
                    }
                });
            });
        }

        private static string WriteValidationException(ValidationException exception, HttpContext context, IResultResponse resultResponse)
        {
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            var errorMessages = new List<string>();
            foreach (var error in exception.Errors)
                errorMessages.Add(error.ErrorMessage);
            var response = resultResponse.Failure(ResponseCodes.FormatOrValidationError, reasons: errorMessages);
            var responseJson = JsonSerializer.Serialize(response, GetJsonOption());
            return responseJson;
        }

        private static string WriteMappingException(CompileException compileException, HttpContext context, IResultResponse resultResponse)
        {
            context.Response.StatusCode = StatusCodes.Status422UnprocessableEntity;
            var response = resultResponse.Failure(ResponseCodes.FormatOrValidationError, StatusCodes.Status422UnprocessableEntity);
            response.Error.Reasons.Add(compileException.Message);
            var responseJson = JsonSerializer.Serialize(response, GetJsonOption());
            return responseJson;
        }

        private static string WriteGenericException(Exception exception, HttpContext context, IResultResponse resultResponse)
        {
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            var response = resultResponse.Failure(ResponseCodes.SystemError, StatusCodes.Status500InternalServerError);
            response.Error.Reasons.Add(exception.Message);
            var responseJson = JsonSerializer.Serialize(response, GetJsonOption());
            return responseJson;
        }

        private static JsonSerializerOptions GetJsonOption()
        {
            return new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
        }
    }
}