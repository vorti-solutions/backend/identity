﻿using Microsoft.AspNetCore.Identity;
using MongoDB.Bson.Serialization.Attributes;

namespace Vorti.Authentication.Domain.Entities
{
    public class ApplicationUserRole : IdentityUserRole<Guid>
    {
        [BsonId]
        public Guid Id { get; set; }
    }
}