﻿using AspNetCore.Identity.MongoDbCore.Models;
using GuardNet;
using Microsoft.AspNetCore.Identity;
using MongoDbGenericRepository.Attributes;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Domain.Common.Enumerations;
using Vorti.Authentication.Domain.Common.ValueObjects;

namespace Vorti.Authentication.Domain.Entities
{
    [CollectionName("Users")]
    public class ApplicationUser : MongoIdentityUser<Guid>
    {
        public string TwoFactorCode { get; private set; }
        public bool TwoStepEnabled { get; private set; }
        public Otp Otp { get; private set; }
        public string Status { get; private set; }
        public bool IsActive { get; private set; }
        public long? LastLogin { get; private set; }
        public long DateCreated { get; private set; }
        public long? DateUpdated { get; private set; }

        public ApplicationUser() { }

        public ApplicationUser(string userName) : base(userName) { _userRoles = []; }

        public ApplicationUser(
            string username,
            string email,
            bool emailConfirmed,
            bool isActive) : this(username)
        {
            Email = email;
            EmailConfirmed = emailConfirmed;
            NormalizedEmail = email.ToUpper();
            IsActive = isActive;
            DateCreated = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            Status = UserSignupStatus.Initiated.DisplayName;
        }

        public ApplicationUser(
            Guid id,
            string username,
            string email,
            bool emailConfirmed,
            bool isActive)
            :
            this(
            username,
            email,
            emailConfirmed,
            isActive)
        {
            base.Id = id;
        }

        private readonly List<ApplicationUserRole> _userRoles;
        public IReadOnlyList<ApplicationUserRole> UserRoles => _userRoles;

        public void HashPassword(string password)
        {
            var passwordHasher = new PasswordHasher<ApplicationUser>();
            PasswordHash = passwordHasher.HashPassword(this, password);
        }

        public IReadOnlyList<ApplicationUserRole> AddToRole(ApplicationRole role)
        {
            Guard.NotNull(role, nameof(role));
            _userRoles.Add(new ApplicationUserRole
            {
                Id = Guid.NewGuid(),
                RoleId = role.Id,
                UserId = Id
            });

            return _userRoles;
        }

        public void UpdatePhoneNumber(string phoneNumner)
        {
            UserName = phoneNumner;
            PhoneNumber = phoneNumner;
        }
        
        public void SetTwoStepEnabled(bool isTwoStepAuth) => TwoStepEnabled = isTwoStepAuth;

        public void SetTwoFactorCode(string userUniqueKey = default) => TwoFactorCode = userUniqueKey;

        public void UpdateEmail(string email) => Email = email;

        public void UserOnboarding() => Status = UserSignupStatus.Onboarded.DisplayName;

        public void InitiateSignup() => Status = UserSignupStatus.Initiated.DisplayName;

        public void UpdateLastModifiedDate() => DateUpdated = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

        public void CompleteSignup() => Status = UserSignupStatus.Completed.DisplayName;

        public void GenerateOtp(IUtility utility, OtpPurpose otpPurpose) 
            => Otp = Otp.Create(otpPurpose, utility, TimeSpan.FromMinutes(15));

        public void RegisterLastLogin() => LastLogin = DateTimeOffset.UtcNow.ToUnixTimeSeconds();

        public void ToggleActivateUser() => IsActive = !IsActive; 

        public void ToggleConfirmEmail() => EmailConfirmed = !EmailConfirmed;

        public void ToggleDeactivateUser() => IsActive = !IsActive;
    }
}