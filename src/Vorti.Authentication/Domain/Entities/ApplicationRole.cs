﻿using AspNetCore.Identity.MongoDbCore.Models;
using MongoDbGenericRepository.Attributes;

namespace Vorti.Authentication.Domain.Entities
{
    [CollectionName("Roles")]
    public class ApplicationRole : MongoIdentityRole<Guid>
    {
        public string Description { get; set; }
        public long DateCreated { get; set; } 
        public long? DateUpdated { get; set; }
    }
}