﻿namespace Vorti.Authentication.Domain.Common.Enumerations
{
    public class OtpPurpose : Enumeration
    {
        public static readonly OtpPurpose Signup = new(1, nameof(Signup));
        public static readonly OtpPurpose Onboarding = new(2, nameof(Onboarding));
        public static readonly OtpPurpose ResetPassword = new(3, nameof(ResetPassword));
        public static readonly OtpPurpose ChangeEmail = new(4, nameof(ChangeEmail));
        public static readonly OtpPurpose Signin = new(5, nameof(Signin));

        protected OtpPurpose() { }

        protected OtpPurpose(int value, string displayName) : base(value, displayName) {}
    }
}