﻿namespace Vorti.Authentication.Domain.Common.Enumerations
{
    public class Role : Enumeration
    {
        public static readonly Role SuperAdmin = new(1, nameof(SuperAdmin));
        public static readonly Role Admin = new(2, nameof(Admin));
        public static readonly Role Customer = new(3, nameof(Customer));
        public static readonly Role Driver = new(4, nameof(Driver));
        public static readonly Role Vendor = new(5, nameof(Vendor));
        public static readonly Role DeliveryAgent = new(6, nameof(DeliveryAgent));

        protected Role() {}

        protected Role(int value, string displayName) : base(value, displayName) {}
    }
}