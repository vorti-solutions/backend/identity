﻿namespace Vorti.Authentication.Domain.Common.Enumerations
{
    public class UserSignupStatus : Enumeration
    {
        public static readonly UserSignupStatus Onboarded = new(1, nameof(Onboarded));
        public static readonly UserSignupStatus Initiated = new(2, nameof(Initiated));
        public static readonly UserSignupStatus Completed = new(3, nameof(Completed));

        protected UserSignupStatus() {}

        protected UserSignupStatus(int value, string displayName) : base(value, displayName) {}
    }
}