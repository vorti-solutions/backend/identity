﻿namespace Vorti.Authentication.Domain.Common.Configs
{
    public class MongoDbConfig
    {
        public string ConnectionString { get; init; }
        public string Database { get; init; }
        public string UserCollectionName { get; init; }
        public string RoleCollectionName { get; init; }
        public string UserRoleCollectionName { get; init; }
    }
}