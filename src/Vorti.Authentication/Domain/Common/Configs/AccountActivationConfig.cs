﻿namespace Vorti.Authentication.Domain.Common.Configs
{
    public class AccountActivationConfig
    {
        public string VendorAccountActivationURL { get; set; }
        public string DriverAccountActivationURL { get; set; }
        public string DeliveryAgencyAccountActivationURL { get; set; }
        public string AdminAccountActivationURL { get; set; }
        public string SuperAccountActivationURL { get; set; } 
    }
}