﻿namespace Vorti.Authentication.Domain.Common.Configs
{
    public class Google2FAConfig
    {
        public string Issuer { get; set; }
        public int QR_PPM { get; set; }
    }
}