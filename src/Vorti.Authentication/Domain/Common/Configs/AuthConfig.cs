﻿namespace Vorti.Authentication.Domain.Common.Configs
{
    public class AuthConfig
    {
        public string Secret { get; set; }
        public string ValidIssuer { get; set; }
        public string ValidAudience { get; set; }
        public int Expiration { get; set; }
    }
}