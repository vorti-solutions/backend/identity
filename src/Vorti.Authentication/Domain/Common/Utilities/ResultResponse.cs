﻿using Vorti.Authentication.Application.Contracts.Utility;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Domain.Common.Utilities
{
    public class ResultResponse : IResultResponse
    {
        public BaseResponse Failure(ResponseCodes responseCode, int statusCode = 400, string responseDescription = null, IList<string> reasons = null)
        {
            return new BaseResponse
            (
                false,
                new ErrorResponse
                (
                    responseCode.GetCode(),
                    string.IsNullOrEmpty(responseDescription) ? responseCode.GetDescription() : responseDescription,
                    reasons ?? []
                ),
                statusCode
            );
        }

        public ServiceResponse<T> Success<T>(T payload = null, int statusCode = 200) where T : class
        {
            return new ServiceResponse<T>(true, statusCode, payload);
        }

        public ServiceResponse Success(int statusCode = 200)
        {
            return new ServiceResponse(true, statusCode);
        }
    }
}