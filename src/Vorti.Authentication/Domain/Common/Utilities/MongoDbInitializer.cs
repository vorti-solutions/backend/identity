﻿using MongoDB.Driver;
using Vorti.Authentication.Domain.Common.Configs;
using Vorti.Authentication.Domain.Entities;
using Vorti.Authentication.Infrastructure.Persistence;

namespace Vorti.Authentication.Domain.Common.Utilities
{
    public class MongoDbInitializer(IMongoClient mongoClient, string databaseName, MongoDbConfig mongoDbConfig)
    {
        private readonly IMongoClient _mongoClient = mongoClient;
        private readonly string _databaseName = databaseName;
        private readonly MongoDbConfig _mongoDbConfig = mongoDbConfig;

        public void InitializeDatabase()
        {
            var databaseList = _mongoClient.ListDatabaseNames().ToList();

            if (!databaseList.Contains(_databaseName))
            {
                IMongoDatabase database = _mongoClient.GetDatabase(_databaseName);
                var collectionList = database.ListCollectionNames().ToList();

                if (!collectionList.Contains(_mongoDbConfig.UserCollectionName))
                {
                    database.CreateCollection(_mongoDbConfig.UserCollectionName);
                    var users = database.GetCollection<ApplicationUser>(_mongoDbConfig.UserCollectionName);
                    RepositoryContextSeed.SeedAppUser(users);
                }

                if (!collectionList.Contains(_mongoDbConfig.RoleCollectionName))
                {
                    database.CreateCollection(_mongoDbConfig.RoleCollectionName);
                    var roles = database.GetCollection<ApplicationRole>(_mongoDbConfig.RoleCollectionName);
                    RepositoryContextSeed.SeedAppRoles(roles);
                }

                if (!collectionList.Contains(_mongoDbConfig.UserRoleCollectionName))
                {
                    database.CreateCollection(_mongoDbConfig.UserRoleCollectionName);
                    var userRoles = database.GetCollection<ApplicationUserRole>(_mongoDbConfig.UserRoleCollectionName);
                    RepositoryContextSeed.SeedIdentityUserRole(userRoles);
                }
            }
        }
    }
}