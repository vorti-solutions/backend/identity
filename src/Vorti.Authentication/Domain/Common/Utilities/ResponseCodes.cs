﻿namespace Vorti.Authentication.Domain.Common.Utilities
{
    public enum ResponseCodes
    {
        [ResponseCodeDescriber("96", "Our service is currently unavailable. Please try again later.")]
        SystemError,

        [ResponseCodeDescriber("03", "Invalid login credentials.")]
        InvalidLoginCredentials,

        [ResponseCodeDescriber("05", "User signup failed.")]
        UserSignupFailure,

        [ResponseCodeDescriber("06", "User account is locked or inactive. Please contact administrator.")]
        UserLockedOrInactive,

        [ResponseCodeDescriber("07", "Email has been taken.")]
        EmailTaken,

        [ResponseCodeDescriber("08", "Otp verification failed.")]
        InvalidOtp,

        [ResponseCodeDescriber("09", "Unable to reset user password or failed attempts.")]
        UserResetFailure,

        [ResponseCodeDescriber("13", "Change password request failed.")]
        ChangePasswordFailure,

        [ResponseCodeDescriber("14", "Change phone number request failed.")]
        ChangePhoneNumberFailure,

        [ResponseCodeDescriber("15", "User account is locked. Please contact administrator.")]
        UserLocked,
        
        [ResponseCodeDescriber("16", "User account does not exist on this platform. Please signup or contact administrator.")]
        InvalidUserAccount,

        [ResponseCodeDescriber("17", "Email notification failed.")]
        SendEmailFailure,

        [ResponseCodeDescriber("18", "Otp notification failed.")]
        SendOtpFailure,

        [ResponseCodeDescriber("19", "Unable to assign role to user.")]
        AssignUserRoleFailure,

        [ResponseCodeDescriber("20", "Unable to update user record.")]
        UpdateRecordError,

        [ResponseCodeDescriber("21", "Change email address request failed.")]
        ChangeEmailFailure,

        [ResponseCodeDescriber("23", "User role does not exist on this platfrom. Please contact administrator.")]
        InvalidRole,

        [ResponseCodeDescriber("25", "Mobile number already exist.")]
        PhoneNumberAlreadyExist,

        [ResponseCodeDescriber("26", "Onboarding user request failed.")]
        UserOnboardingFailure,

        [ResponseCodeDescriber("27", "User signin failed.")]
        SignInFailure,

        [ResponseCodeDescriber("29", "Invalid otp template. Please contact administrator.")]
        InvalidOtpTemplate,

        [ResponseCodeDescriber("30", "Format or validation error.")]
        FormatOrValidationError,

        [ResponseCodeDescriber("31", "Deactivating user request failed.")]
        DeactivateUserFailure,

        [ResponseCodeDescriber("32", "Activating user request failed.")]
        ActivateUserFailure,

        [ResponseCodeDescriber("35", "Invalid two factor code.")]
        Invalid2FACode
    }

    [AttributeUsage(AttributeTargets.Field, Inherited = false)]
    sealed class ResponseCodeDescriberAttribute(string code, string description) : Attribute
    {
        public string Code { get; } = code;
        public string Description { get; } = description;
    }

    public static class ResponseCodeExtension
    {
        public static string GetCode(this ResponseCodes responseCode)
        {
            var type = typeof(ResponseCodes);
            var property = type.GetField(responseCode.ToString());
            var attribute = (ResponseCodeDescriberAttribute[])property.GetCustomAttributes(typeof(ResponseCodeDescriberAttribute), false);
            return attribute[0].Code;
        }

        public static string GetDescription(this ResponseCodes responseCode)
        {
            var type = typeof(ResponseCodes);
            var property = type.GetField(responseCode.ToString());
            var attribute = (ResponseCodeDescriberAttribute[])property.GetCustomAttributes(typeof(ResponseCodeDescriberAttribute), false);
            return attribute[0].Description;
        }
    }
}