﻿namespace Vorti.Authentication.Domain.Common.Utilities
{
    public class Routes
    {
        #region
        public const string Base = "v{version:apiVersion}/[controller]";
        #endregion

        #region Send Otp  
        public const string SEND_OTP = "otp";
        #endregion

        #region Send Email  
        public const string SEND_EMAIL = "email";
        #endregion

        #region Activate User
        public const string ACTIVATE_USER = "activate";
        #endregion

        #region Deactivate User
        public const string DEACTIVATE_USER = "deactivate";
        #endregion

        #region Login
        public const string LOGIN = "signin";
        public const string LOGIN_MULTI_FACTOR = "signin/mfa";
        #endregion

        #region Chane Email
        public const string INITIATE_EMAIL_CHANGE = "initiate/change/email";
        public const string EMAIL_CHANGE = "change/email";
        #endregion

        #region Reset Password
        public const string INITIATE_RESET_PASSWORD = "initiate/reset/password";
        public const string RESET_PASSWORD = "reset/password";
        #endregion

        #region Change Password
        public const string CHANGE_PASSWORD = "change/password";
        #endregion

        #region Multi Factor Authentication Setup
        public const string SET_MULTI_FACTOR = "set/mfa";
        public const string MULTI_FACTOR_STATUS = "status/mfa";
        #endregion

        #region Onboard Vendor, Delivery Agent and Driver  
        public const string ONBOARD_USER = "onboarding";
        #endregion

        #region Supper Admin, Admin, Vendor, Driver, Customer and Delivery Agent
        public const string INITIATE_SIGNUP = "initiate/signup";
        public const string COMPLETE_SIGNUP = "signup";
        #endregion

        #region Customer
        public const string CUSTOMERS = "";
        public const string CUSTOMER_DETAILS_BY_EMAIL = "info/{email}";
        public const string CUSTOMER_DETAILS = "info";
        public const string UPDATE_DETAILS = "edit";
        #endregion

        #region Supper Admin, Admin, Vendor, Driver, Customer and Delivery Agent Contact Change
        public const string CHANGE_PHONE_NUMBER = "profile/phoneNumber";
        #endregion

        #region Delivery Address
        public const string ADDRESS = "";
        public const string ADDRESSES = "{customerId}";
        public const string CREATE_ADDRESS = "add";
        public const string UPDATE_ADDRESS = "edit";
        public const string DELETE_ADDRESS = "remove";
        public const string DEFAULT_ADDRESS = "default";
        #endregion

        #region Nigerian States
        public const string NIGERIAN_STATE = "state";
        public const string NIGERIAN_STATES = "states";
        #endregion
    }
}