﻿using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using Vorti.Authentication.Application.Contracts.Utility;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using Vorti.Authentication.Domain.Common.Configs;
using VortifySecure;
using Microsoft.Extensions.Options;

namespace Vorti.Authentication.Domain.Common.Utilities
{
    public class Utility(IOptions<AppConfig> appConfig) : IUtility
    {
        private readonly AppConfig _appConfig = appConfig.Value;

        public string ComputeHmacSha512(string dataToBeHashed, string key = null)
        {
            var dataBytes = Encoding.UTF8.GetBytes(dataToBeHashed);
            var hashKeyBytes = string.IsNullOrWhiteSpace(key) ? Encoding.UTF8.GetBytes(string.Empty) : Encoding.UTF8.GetBytes(key);
            using var hmac = new HMACSHA512(hashKeyBytes);
            var resultBytes = hmac.ComputeHash(dataBytes);
            var resultString = Helper.ConvertByteArrayToString(resultBytes);
            return resultString;
        }

        public string ComputeSHA512(string data)
        {
            var byteData = Encoding.UTF8.GetBytes(data);
            var hashedBytes = SHA512.HashData(byteData);
            string hash = Helper.ConvertByteArrayToString(hashedBytes);
            return hash;
        }

        public string HashWithSalt(string rawText, string salt = null)
        {
            var saltBytes = !string.IsNullOrWhiteSpace(salt) ? Convert.FromBase64String(salt) : GenerateSalt();
            var toBeHashed = Encoding.UTF8.GetBytes(rawText);
            using var rfc2898 = new Rfc2898DeriveBytes(toBeHashed, saltBytes, 10000, HashAlgorithmName.SHA512);
            var resultBytes = rfc2898.GetBytes(32);
            var hashString = $"{Convert.ToBase64String(resultBytes)}|{Convert.ToBase64String(saltBytes)}";
            return hashString;
        }

        private static byte[] GenerateSalt()
        {
            var randomNumber = new byte[32];
            using var randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(randomNumber);
            return randomNumber;
        }

        public TRequest DecryptRequest<TRequest>(RequestPayload request) where TRequest : class
        {
            TRequest result = default;
            if (request != null && !string.IsNullOrWhiteSpace(request.EncryptedPayload))
            {
                string decryptedRequest = new AES(_appConfig.AesKey).Decrypt(request.EncryptedPayload);
                result = JsonSerializer.Deserialize<TRequest>(decryptedRequest, GetJsonOption());
            }

            return result;
        }

        private static JsonSerializerOptions GetJsonOption()
        {
            return new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
        }
    }
}