﻿using System.Text;

namespace Vorti.Authentication.Domain.Common.Utilities
{
    public class Helper
    {
        private static readonly Random random = new();

        public static string ConvertByteArrayToString(byte[] bytes)
        {
            StringBuilder builder = new();
            for (int i = 0; i < bytes.Length; i++)
                builder.Append(bytes[i].ToString("x2"));

            return builder.ToString();
        }

        public static string GenerateDefaultPassword(int length)
        {
            if (length <= 0) throw new ArgumentException("Length must be greater than zero.", nameof(length));

            const string uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string lowercase = "abcdefghijklmnopqrstuvwxyz";
            const string numbers = "0123456789";
            const string specialChars = "!*@#$%^&+=";//!@#$%^&*()_-+=<>?{}|~
            const string allChars = uppercase + lowercase + numbers + specialChars;

            var stringBuilder = new StringBuilder(length);

            // Ensure at least one of each required character type
            stringBuilder.Append(SelectRandomChar(uppercase));
            stringBuilder.Append(SelectRandomChar(lowercase));
            stringBuilder.Append(SelectRandomChar(numbers));
            stringBuilder.Append(SelectRandomChar(specialChars));

            // Fill the rest of the password with random characters from all sets
            for (int i = 4; i < length; i++)
                stringBuilder.Append(SelectRandomChar(allChars));

            return ShuffleString(stringBuilder.ToString());
        }

        private static char SelectRandomChar(string charSet)
        {
            int randomIndex = random.Next(charSet.Length);
            return charSet[randomIndex];
        }

        // Helper method to shuffle a string (Fisher-Yates shuffle algorithm)
        private static string ShuffleString(string input)
        {
            char[] array = input.ToCharArray();
            Random rng = new();

            for (int i = array.Length - 1; i > 0; i--)
            {
                int j = rng.Next(i + 1);
                (array[j], array[i]) = (array[i], array[j]);
            }

            return new string(array);
        }
    }
}