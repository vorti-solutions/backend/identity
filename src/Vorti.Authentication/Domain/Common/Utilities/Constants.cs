﻿namespace Vorti.Authentication.Domain.Common.Utilities
{
    public static class Constants
    {
        public static class NotificationTemplate
        {
            public const string FOLDER = "NotificationTemplates";
            public const string RESET = "Vortify password assistance";
            public const string VENDOR_ONBOARDING = "Vortify vendor onboarding";
            public const string DRIVER_ONBOARDING = "Vortify driver onboarding";
            public const string DELIVERY_AGENCY_ONBOARDING = "Vortify delivery agency onboarding";
            public const string SUPER_ADMIN_ONBOARDING = "Vortify super admin onboarding";
            public const string ADMIN_ONBOARDING = "Vortify admin onboarding";
            public const string SIGNUP = "Verify your new vortify account";
            public const string SIGNIN = "Verify your account";
            public const string CHANGE_EMAIL = "Account holder email change";

            public static class OtpTemplate
            { 
                public const string RESET_OTP_TEMPLATE = "reset_otp.html";
                public const string SIGNUP_OTP_TEMPLATE = "signup_otp.html";
                public const string SIGNIN_OTP_TEMPLATE = "signin_otp.html";
                public const string ONBOARDING_OTP_TEMPLATE = "onboarding_otp.html";
                public const string CHANGE_EMAIL_OTP_TEMPLATE = "change_email_otp.html";
                public static class Placeholders
                {
                    public const string OTP = "{{otp}}";
                    public const string YEAR = "{{year}}";
                    public const string USERNAME = "{{username}}"; 
                    public const string ONBOARDING_EMAIL = "{{onboardingEmail}}";
                    public const string ONBOARDING_PASSWORD = "{{onboardingPassword}}";
                    public const string ACCOUNT_ACTIVATION_URL = "{{accountActivationUrl}}";
                }
            }
        }

        public static class AppRegex
        {
            public const string PASSWORD = @"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=]).*$";
            public const string PHONE_NUMBER = @"^\+?\d{10,15}$";// @"^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$";
            public const string ALPHANUMERIC_WITH_COMMA = @"^[A-Za-z0-9\-_\s\,]+$";
            public const string ALPHANUMERIC = @"^[A-Za-z0-9\-_\s]+$";
            public const string ALPHABET = @"^([^0-9]*)$";
            public const string DIGIT = @"^[0-9]+$";
            public const string COUNTRY_CODE = @"^\+?\d{1,4}$";// @"^(\+?[0-9]*)$";
            public const string EMAIL = @"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";
            public const string UPPERCASE_LETTER = @"[A-Z]";
            public const string LOWERCASE_LETTER = @"[[a-z]]";
            public const string NUMBER = @"[0-9]";
            public const string SPECIAL_CHARACTER = @"[\!\@\#\$\%\^\&\*\(\)\-\+\=]";
        }

        public static class Util
        {
            public const string FORM_DATA = "multipart/form-data";
            public const string BEARER_FORMAT = "JWT";
            public const string BEARER_DESCRIPTION = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.";
        }

        public static class SysRole
        {
            public const string ADMIN = "Admin";
            public const string DRIVER = "Driver";
            public const string VENDOR = "Vendor";
            public const string CUSTOMER = "Customer";
            public const string SUPER_ADMIN = "SuperAdmin";
            public const string DELIVERY_AGENT = "DeliveryAgent"; 
        }
    }
}