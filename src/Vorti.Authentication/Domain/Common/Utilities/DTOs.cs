﻿namespace Vorti.Authentication.Domain.Common.Utilities
{
    public class DTOs
    {
        public record BaseResponse(bool IsSuccess, ErrorResponse Error, int StatusCode);
        public record ErrorResponse(string ErrorCode, string Description, IList<string> Reasons = default);
        public record ServiceResponse<T>(bool IsSuccess, int StatusCode, T Payload) : BaseResponse(IsSuccess, default, StatusCode);
        public record ServiceResponse(bool IsSuccess, int StatusCode) : BaseResponse(IsSuccess, default, StatusCode);

        public record RequestPayload(string EncryptedPayload);
    }
}