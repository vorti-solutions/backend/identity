﻿using GuardNet;
using System.Text.Json;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Domain.Common.Enumerations;

namespace Vorti.Authentication.Domain.Common.ValueObjects
{
    public class Otp : ValueObject
    {
        private string _code;
        const string numericChars = "0123456789";

        public string Hash { get; private set; }
        public OtpPurpose Purpose { get; private set; }
        public DateTime? Expiry { get; private set; }
        public bool IsUsed { get; private set; }

        private Otp(OtpPurpose purpose, IUtility utility, string code = default, string salt = default)
        {
            Guard.NotNull(purpose, nameof(purpose));

            if (string.IsNullOrEmpty(code))
                GenerateOtpCode();
            else
                _code = code;

            Guard.NotNullOrWhitespace(_code, nameof(_code));
            Hash = utility.HashWithSalt(_code, salt);
            Purpose = purpose;
            IsUsed = false;
        }

        private Otp(OtpPurpose purpose, IUtility utility, TimeSpan validTill, string code = default, string salt = default)
            :this(purpose, utility, code, salt)
        {
            Expiry = DateTime.UtcNow + validTill;
        }

        public static Otp Create(OtpPurpose purpose, IUtility utility, TimeSpan validTill, string code = default, string salt = default) 
            => new(purpose, utility, validTill, code, salt);

        private void GenerateOtpCode(int length = 6)
        {
            _code = new string(Enumerable.Repeat(numericChars, length)
                .Select(x => x[new Random().Next(x.Length)]).ToArray());
        }

        public string GetOtp() => _code;

        public (bool, string) Verify(string rawOtp, IUtility utility)
        {
            if (this == null) return (false, "otp is invalid");
            if (IsUsed) return (false, "otp has been used");

            var salt = this?.Hash.Split('|').Last();
            var otp = new Otp(Purpose, utility, rawOtp, salt);
            if (this != otp) return (false, "otp is invalid");

            if (DateTime.Compare(DateTime.UtcNow, (DateTime)Expiry) > 0)
                return (false, "otp is expired");

            IsUsed = true;
            return (true, null);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Hash;
            yield return Purpose.Value;
        }

        public override string ToString() => JsonSerializer.Serialize(this);
    }
}