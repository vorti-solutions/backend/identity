﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ActivateUser.Commands
{
    public class ActivateUserCommand(Guid userId) : IRequest<BaseResponse>
    {
        public Guid UserId { get; set; } = userId;
    }
}