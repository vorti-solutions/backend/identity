﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ActivateUser.Commands
{
    public class ActivateUserCommandHandler(IUserIdentityService identityService) : IRequestHandler<ActivateUserCommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(ActivateUserCommand request, CancellationToken cancellationToken)
        {
            return await _identityService.ActivateAsync(request.UserId);
        }
    }
}