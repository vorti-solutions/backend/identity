﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Notifications.Otp.Commands
{
    public class OtpCommandHandler(IUserIdentityService identityService) : IRequestHandler<OtpCommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(OtpCommand request, CancellationToken cancellationToken)
        {
            return await _identityService.SendOtpAsync(request.Email, request.OtpType);
        }
    }
}