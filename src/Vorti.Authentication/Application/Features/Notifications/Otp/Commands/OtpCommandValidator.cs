﻿using FluentValidation;
using FluentValidation.Validators;

namespace Vorti.Authentication.Application.Features.Notifications.Otp.Commands
{
    public class OtpCommandValidator : AbstractValidator<OtpCommand>
    {
        public OtpCommandValidator()
        {
            RuleFor(e => e.Email)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(o => o.OtpType)
                .InclusiveBetween(1, 5);
        }
    }
}