﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Notifications.Otp.Commands
{
    public class OtpCommand : IRequest<BaseResponse>
    {
        public string Email { get; set; }
        public int OtpType { get; set; }
    }
}