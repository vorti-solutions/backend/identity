﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Notifications.Email.Commands
{
    public class EmailCommand : IRequest<BaseResponse>
    {
        public string ToAddress { get; set; }
        public string Subject { get; set; }
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public string HtmlBody { get; set; }
        public string StringBody { get; set; }
        public bool IsHtmlBody { get; set; }
        public IFormFileCollection Attachments { get; set; } = default;
    }
}