﻿using FluentValidation;
using FluentValidation.Validators;

namespace Vorti.Authentication.Application.Features.Notifications.Email.Commands
{
    public class EmailCommandValidator : AbstractValidator<EmailCommand>
    {
        public EmailCommandValidator()
        {
            RuleFor(x => x.ToAddress)
            .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(x => x.Subject)
            .NotEmpty();

            When(x => x.IsHtmlBody, () =>
            {
                RuleFor(x => x.HtmlBody)
                .NotEmpty();
            }).Otherwise(() =>
            {
                RuleFor(x => x.StringBody)
                .NotEmpty();
            });

            RuleFor(x => x)
                .Must(HaveHtmlBodyOrStringBody)
                .WithMessage("Either htmlBody or stringBody is required.");
        }

        private bool HaveHtmlBodyOrStringBody(EmailCommand command)
        {
            return !string.IsNullOrEmpty(command.HtmlBody) || !string.IsNullOrEmpty(command.StringBody);
        }
    }
}