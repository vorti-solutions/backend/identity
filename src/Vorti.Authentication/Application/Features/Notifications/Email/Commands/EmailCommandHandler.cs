﻿using MapsterMapper;
using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Notification;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Domain.Common.Utilities;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Application.Features.Notifications.Email.Commands
{
    public class EmailCommandHandler(INotificationService notificationService, IResultResponse resultResponse, IMapper mapper) : IRequestHandler<EmailCommand, BaseResponse>
    {
        private readonly INotificationService _notificationService = notificationService;
        private readonly IResultResponse _resultResponse = resultResponse;
        private readonly IMapper _mapper = mapper;

        public async Task<BaseResponse> Handle(EmailCommand request, CancellationToken cancellationToken)
        {
            var emailRequest = _mapper.Map<EmailRequest>(request);

            if (request.Attachments != null && request.Attachments.Count != 0)
            {
                List<Stream> attachments = [];
                foreach (var attachment in request.Attachments)
                {
                    var attachmentTempPath = Path.GetTempFileName();
                    using var stream = File.Create(attachmentTempPath);
                    await attachment.CopyToAsync(stream, cancellationToken);
                    stream.Dispose();

                    using var ms = new MemoryStream();
                    using var fs = File.OpenRead(attachmentTempPath);
                    await fs.CopyToAsync(ms, cancellationToken);
                    attachments.Add(ms);
                }
                emailRequest.Attachments = attachments;
            }

            var emailResult = await _notificationService.SendSmtpEmail(emailRequest);
            if (!emailResult)
            {
                return _resultResponse.Failure(
                    ResponseCodes.SendEmailFailure, 
                    StatusCodes.Status422UnprocessableEntity);
            }

            return _resultResponse.Success();
        }
    }
}