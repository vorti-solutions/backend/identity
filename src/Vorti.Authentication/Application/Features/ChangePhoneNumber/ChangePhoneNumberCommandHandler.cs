﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Application.Features.ChangePhoneNumber
{
    public class ChangePhoneNumberCommandHandler(
        IHttpContextAccessor httpContextAccessor,
        IUserIdentityService identityService) : IRequestHandler<ChangePhoneNumberCommand, BaseResponse>
    {
        private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(ChangePhoneNumberCommand request, CancellationToken cancellationToken)
        {
            var userClaimsResult = await _identityService.GetUserFromClaimsAsync(_httpContextAccessor.HttpContext.User);
            if (!userClaimsResult.IsSuccess)
                return userClaimsResult;

            var user = (userClaimsResult as ServiceResponse<UserResponse>).Payload.User;

            return await _identityService.UpdatePhoneNumberAsync(user, request.NewPhoneNumber);
        }
    }
}