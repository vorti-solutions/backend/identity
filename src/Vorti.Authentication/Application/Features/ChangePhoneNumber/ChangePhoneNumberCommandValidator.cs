﻿using FluentValidation;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.ChangePhoneNumber
{
    public class ChangePhoneNumberCommandValidator : AbstractValidator<ChangePhoneNumberCommand>
    {
        public ChangePhoneNumberCommandValidator()
        {
            RuleFor(p => p.NewPhoneNumber)
                .NotEmpty()
                .Matches(Constants.AppRegex.PHONE_NUMBER);
        }
    }
}