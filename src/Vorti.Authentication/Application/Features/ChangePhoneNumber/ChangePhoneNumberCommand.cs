﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ChangePhoneNumber
{
    public class ChangePhoneNumberCommand(string phoneNumber) : IRequest<BaseResponse>
    {
        public string NewPhoneNumber { get; set; } = phoneNumber;
    }
}