﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Domain.Entities;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Users.UserCreation.Commands
{
    public class CompleteUserSignupCommandHandler(
        UserManager<ApplicationUser> userManager,
        IResultResponse resultResponse,
        IUtility utility) : IRequestHandler<CompleteUserSignupCommand, BaseResponse>
    {
        private readonly UserManager<ApplicationUser> _userManager = userManager;
        private readonly IResultResponse _resultResponse = resultResponse;
        private readonly IUtility _utility = utility;

        public async Task<BaseResponse> Handle(CompleteUserSignupCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                return _resultResponse.Failure(ResponseCodes.InvalidUserAccount);
            }

            var otpResult = user.Otp.Verify(request.Otp, _utility);
            if (!otpResult.Item1)
            {
                return _resultResponse.Failure(
                    ResponseCodes.InvalidOtp,
                    reasons: [otpResult.Item2]);
            }

            user.ToggleActivateUser();
            user.ToggleConfirmEmail();
            user.CompleteSignup();
            user.UpdateLastModifiedDate();

            var userUpdateResult = await _userManager.UpdateAsync(user);
            if (!userUpdateResult.Succeeded)
            {
                return _resultResponse.Failure(
                    ResponseCodes.UserSignupFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: userUpdateResult.Errors.Select(x => x.Description).ToList());
            }

            return _resultResponse.Success();
        }
    }
}