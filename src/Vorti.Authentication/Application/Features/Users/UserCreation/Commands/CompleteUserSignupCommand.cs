﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Users.UserCreation.Commands
{
    public class CompleteUserSignupCommand : IRequest<BaseResponse>
    {
        public string Email { get; set; }
        public string Otp { get; set; }
    }
}