﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Domain.Entities;
using Vorti.Authentication.Domain.Common.Enumerations;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Users.UserCreation.Commands
{
    public class InitiateUserSignupCommandHandler(
        IUserIdentityService identityService,
        UserManager<ApplicationUser> userManager,
        IResultResponse resultResponse) : IRequestHandler<InitiateUserSignupCommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;
        private readonly UserManager<ApplicationUser> _userManager = userManager;
        private readonly IResultResponse _resultResponse = resultResponse;

        public async Task<BaseResponse> Handle(InitiateUserSignupCommand request, CancellationToken cancellationToken)
        {
            if (request.Onboarded)
            {
                var user = await _userManager.FindByEmailAsync(request.Email);
                if (user == null)
                {
                    return _resultResponse.Failure(ResponseCodes.InvalidUserAccount);
                }

                var userSignupPasswordResult = await _userManager.ChangePasswordAsync(user, request.DefaultPassword, request.NewPassword);
                if (!userSignupPasswordResult.Succeeded)
                {
                    return _resultResponse.Failure(
                        ResponseCodes.ChangePasswordFailure,
                        reasons: userSignupPasswordResult.Errors.Select(x => x.Description).ToList());
                }

                user.InitiateSignup();

                var updateUserResult = await _userManager.UpdateAsync(user);
                if (!updateUserResult.Succeeded)
                {
                    return _resultResponse.Failure(
                        ResponseCodes.UpdateRecordError,
                        StatusCodes.Status422UnprocessableEntity,
                        reasons: updateUserResult.Errors.Select(x => x.Description).ToList());
                }

                var sendOtpResult = await _identityService.SendOtpAsync(user.Email, OtpPurpose.Signup);
                if (!sendOtpResult.IsSuccess)
                {
                    return sendOtpResult;
                }
            }
            else
            {
                return await _identityService.RegisterCustomerUserAsync(request.Email, request.NewPassword, request.PhoneNumber, cancellationToken);
            }

            return _resultResponse.Success();
        }
    }
}