﻿using FluentValidation;
using FluentValidation.Validators;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Users.UserCreation.Commands
{
    public class CompleteUserSignupCommandValidator : AbstractValidator<CompleteUserSignupCommand>
    {
        public CompleteUserSignupCommandValidator()
        {
            RuleFor(e => e.Email)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(x => x.Otp)
            .Matches(Constants.AppRegex.DIGIT);
        }
    }
}