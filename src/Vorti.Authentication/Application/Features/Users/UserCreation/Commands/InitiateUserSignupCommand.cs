﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Users.UserCreation.Commands
{
    public class InitiateUserSignupCommand : IRequest<BaseResponse>
    {
        public bool Onboarded { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string DefaultPassword { get; set; }
        public string NewPassword { get; set; }
    }
}