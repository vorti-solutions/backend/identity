﻿using FluentValidation;
using FluentValidation.Validators;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Users.UserCreation.Commands
{
    public class InitiateUserSignupCommandValidator : AbstractValidator<InitiateUserSignupCommand>
    {
        public InitiateUserSignupCommandValidator()
        {
            RuleFor(e => e.Email)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(np => np.NewPassword)
                .NotEmpty()
                .MinimumLength(8)
                .Matches(Constants.AppRegex.PASSWORD);

            When(x => x.Onboarded, () =>
            {
                RuleFor(x => x.DefaultPassword)
                .NotEmpty()
                .Length(8)
                .Matches(Constants.AppRegex.PASSWORD);
            }).Otherwise(() =>
            {
                RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .Matches(Constants.AppRegex.PHONE_NUMBER);
            });

            RuleFor(x => x)
                .Must(HaveDefaultPasswordOrPhoneNumber)
                .WithMessage("Either default password or mobile phone number is required.");
        }

        private bool HaveDefaultPasswordOrPhoneNumber(InitiateUserSignupCommand command)
        {
            return !string.IsNullOrEmpty(command.DefaultPassword) || !string.IsNullOrEmpty(command.PhoneNumber);
        }
    }
}