﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Users.UserOnboarding.Commands
{
    public class UserOnboardingCommand : IRequest<BaseResponse>
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int RoleType { get; set; }
    }
}