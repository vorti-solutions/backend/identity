﻿using FluentValidation;
using FluentValidation.Validators;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Users.UserOnboarding.Commands
{
    public class UserOnboardingCommandValidator : AbstractValidator<UserOnboardingCommand>
    {
        public UserOnboardingCommandValidator()
        {
            RuleFor(e => e.Email)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(p => p.PhoneNumber)
                .Matches(Constants.AppRegex.PHONE_NUMBER);

            RuleFor(o => o.RoleType)
                .InclusiveBetween(1, 6);
        }
    }
}