﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;
using Vorti.Authentication.Domain.Common.Enumerations;
using Microsoft.AspNetCore.Identity;
using Vorti.Authentication.Domain.Entities;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Users.UserOnboarding.Commands
{
    public class UserOnboardingCommandHandler(
        ILogger<UserOnboardingCommandHandler> logger,
        IUserIdentityService identityService,
        UserManager<ApplicationUser> userManager,
        IResultResponse resultResponse) : IRequestHandler<UserOnboardingCommand, BaseResponse>
    {
        private readonly ILogger<UserOnboardingCommandHandler> _logger = logger;
        private readonly IUserIdentityService _identityService = identityService;
        private readonly UserManager<ApplicationUser> _userManager = userManager;
        private readonly IResultResponse _resultResponse = resultResponse;

        public async Task<BaseResponse> Handle(UserOnboardingCommand request, CancellationToken cancellationToken)
        {
            var role = Enumeration.FromValue<Role>(request.RoleType);

            var onboardingResult = await _identityService.OnboardUserAsync(request.Email, request.PhoneNumber, role.DisplayName, cancellationToken);
            if (!onboardingResult.IsSuccess)
                return onboardingResult;

            var userResp = (onboardingResult as ServiceResponse<UserResponse>).Payload;
            var otpResult = await _identityService.SendOtpAsync(request.Email, OtpPurpose.Onboarding, role, defaultPassword: userResp.DefaultPassword);
            if (!otpResult.IsSuccess)
            {
                _logger.LogInformation($"Could not send otp notification to onboarded User with userId: {userResp.User.Id}");

                try
                {
                    _ = _userManager.DeleteAsync(userResp.User);
                }
                catch (Exception ex)
                {
                    _logger.LogInformation($"Failed to delete userId: '{userResp.User.Id}' after a failed notification.");
                    _logger.LogInformation($"Error message: '{ex.Message}'");
                }

                return _resultResponse.Failure(
                    ResponseCodes.UserOnboardingFailure,
                    StatusCodes.Status422UnprocessableEntity);
            }

            return _resultResponse.Success(new UserIdResponse(userResp.User.Id), StatusCodes.Status201Created);
        }
    }
}