﻿namespace Vorti.Authentication.Application.Features.Login.Commands
{
    public class LoginResponse(bool multiFactor = false, string access_token = default, DateTime? lastLogin = default)
    {
        public bool MultiFactorEnabled { get; set; } = multiFactor;
        public string Access_token { get; set; } = access_token;
        public DateTime? LastLogin { get; set; } = lastLogin;
    }
}