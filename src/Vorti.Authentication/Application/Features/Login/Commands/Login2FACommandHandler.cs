﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Application.Features.Login.Commands
{
    public class Login2FACommandHandler(IUserIdentityService identityService) : IRequestHandler<Login2FACommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(Login2FACommand request, CancellationToken cancellationToken)
        {
            var userClaimsResult = await _identityService.GetUserByEmailAsync(request.Email);
            if (!userClaimsResult.IsSuccess)
                return userClaimsResult;

            var user = (userClaimsResult as ServiceResponse<UserResponse>).Payload.User;
            if (user != null && user.TwoStepEnabled)
            {
                var otpResult = await _identityService.VerifyOtpAsync(request.Email, request.PassCode);
                if (!otpResult.IsSuccess) return otpResult;
            }

            if (user != null && user.TwoFactorEnabled)
            {
                var _2faResult = await _identityService.VerifyTwoFactorAsync(request.Email, request.PassCode);
                if (!_2faResult.IsSuccess) return _2faResult;
            }

            return await _identityService.AuthenticateAsync(user.Id);
        }
    }
}