﻿using FluentValidation;
using FluentValidation.Validators;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Login.Commands
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(x => x.Password)
                .NotEmpty()
                .MinimumLength(8)
                .Matches(Constants.AppRegex.PASSWORD);

            When(x => x.IsEmail, () =>
            {
                RuleFor(x => x.Email)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);
            }).Otherwise(() =>
            {
                RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .Matches(Constants.AppRegex.PHONE_NUMBER);
            });

            RuleFor(x => x)
                .Must(HaveEmailOrPhoneNumber)
                .WithMessage("Either email or mobile phone number is required.");
        }

        private bool HaveEmailOrPhoneNumber(LoginCommand command)
        {
            return !string.IsNullOrEmpty(command.Email) || !string.IsNullOrEmpty(command.PhoneNumber);
        }
    }
}