﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Login.Commands
{
    public class LoginCommandHandler(IUserIdentityService identityService) : IRequestHandler<LoginCommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            return await _identityService.AuthenticateAsync(request);
        }
    }
}