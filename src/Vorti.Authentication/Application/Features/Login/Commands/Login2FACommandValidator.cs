﻿using FluentValidation;
using FluentValidation.Validators;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.Login.Commands
{
    public class Login2FACommandValidator : AbstractValidator<Login2FACommand>
    {
        public Login2FACommandValidator()
        {
            RuleFor(x => x.Email)
            .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(x => x.PassCode)
                .NotEmpty()
            .Matches(Constants.AppRegex.DIGIT)
            .MaximumLength(6);
        }
    }
}