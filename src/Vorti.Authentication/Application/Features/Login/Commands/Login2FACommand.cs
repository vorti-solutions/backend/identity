﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Login.Commands
{
    public class Login2FACommand : IRequest<BaseResponse>
    {
        public string Email { get; set; }
        public string PassCode { get; set; }
    }
}