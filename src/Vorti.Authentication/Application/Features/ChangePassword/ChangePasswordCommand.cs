﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ChangePassword
{
    public class ChangePasswordCommand : IRequest<BaseResponse>
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}