﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Application.Features.ChangePassword
{
    public class ChangePasswordCommandHandler(
        IHttpContextAccessor httpContextAccessor,
        IUserIdentityService identityService) : IRequestHandler<ChangePasswordCommand, BaseResponse>
    {
        private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(ChangePasswordCommand request, CancellationToken cancellationToken)
        {
            var userResult = await _identityService.GetUserFromClaimsAsync(_httpContextAccessor.HttpContext.User);
            if (userResult == null)
            {
                return userResult;
            }

            var user = (userResult as ServiceResponse<UserResponse>)?.Payload.User;
            return await _identityService.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);
        }
    }
}