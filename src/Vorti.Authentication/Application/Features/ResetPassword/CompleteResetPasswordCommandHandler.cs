﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ResetPassword
{
    public class CompleteResetPasswordCommandHandler(IUserIdentityService identityService) : IRequestHandler<CompleteResetPasswordCommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(CompleteResetPasswordCommand request, CancellationToken cancellationToken)
        {
            return await _identityService.ResetPasswordAsync(request);
        }
    }
}