﻿using FluentValidation;
using FluentValidation.Validators;

namespace Vorti.Authentication.Application.Features.ResetPassword
{
    public class InitiateResetPasswordCommandValidator : AbstractValidator<InitiateResetPasswordCommand>
    {
        public InitiateResetPasswordCommandValidator()
        {
            RuleFor(x => x.Email)
            .EmailAddress(EmailValidationMode.AspNetCoreCompatible);
        }
    }
}