﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using Vorti.Authentication.Domain.Common.Enumerations;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ResetPassword
{
    public class InitiateResetPasswordCommandHandler(IUserIdentityService identityService) : IRequestHandler<InitiateResetPasswordCommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(InitiateResetPasswordCommand request, CancellationToken cancellationToken)
        {
            return await _identityService.SendOtpAsync(request.Email, OtpPurpose.ResetPassword);
        }
    }
}