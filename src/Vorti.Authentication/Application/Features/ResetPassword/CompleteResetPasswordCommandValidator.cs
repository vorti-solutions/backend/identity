﻿using FluentValidation;
using FluentValidation.Validators;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.ResetPassword
{
    public class CompleteResetPasswordCommandValidator : AbstractValidator<CompleteResetPasswordCommand>
    {
        public CompleteResetPasswordCommandValidator()
        {
            RuleFor(x => x.Email)
            .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(x => x.NewPassword)
                .NotEmpty()
            .Matches(Constants.AppRegex.PASSWORD);

            RuleFor(x => x.Otp)
                .NotEmpty()
            .Matches(Constants.AppRegex.DIGIT)
            .MaximumLength(6);
        }
    }
}