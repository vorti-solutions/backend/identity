﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ResetPassword
{
    public class InitiateResetPasswordCommand(string email) : IRequest<BaseResponse>
    {
        public string Email { get; set; } = email;
    }
}