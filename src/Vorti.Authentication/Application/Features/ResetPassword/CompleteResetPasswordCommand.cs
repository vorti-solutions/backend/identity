﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ResetPassword
{
    public class CompleteResetPasswordCommand : IRequest<BaseResponse>
    {
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string Otp { get; set; }
    }
}