﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.DeactivateUser.Commands
{
    public class DeactivateUserCommand(Guid userId) : IRequest<BaseResponse>
    {
        public Guid UserId { get; set; } = userId;
    }
}