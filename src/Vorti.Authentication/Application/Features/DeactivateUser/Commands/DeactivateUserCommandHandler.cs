﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.DeactivateUser.Commands
{
    public class DeactivateUserCommandHandler(IUserIdentityService identityService) : IRequestHandler<DeactivateUserCommand, BaseResponse>
    {
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(DeactivateUserCommand request, CancellationToken cancellationToken)
        {
            return await _identityService.DeactivateAsync(request.UserId);
        }
    }
}