﻿using FluentValidation;

namespace Vorti.Authentication.Application.Features.Set2FA.Commands
{
    public class SetMultiFactorCommandValidator : AbstractValidator<SetMultiFactorCommand>
    {
        public SetMultiFactorCommandValidator()
        {

            When(x => x.MultifactorEnabled, () =>
            {
                RuleFor(x => x.TwoStepEnabled)
                .Must(x => x == true)
                .When(x => !x.TwoFactorEnabled)
                .WithMessage("Two step is required.");

                RuleFor(x => x.TwoFactorEnabled)
                .Must(x => x == true)
                .When(x => !x.TwoStepEnabled)
                .WithMessage("Two factor is required.");
            }).Otherwise(() =>
            {
                RuleFor(x => x)
                .Must(x => !x.TwoStepEnabled && !x.TwoFactorEnabled)
                .WithMessage("Multi-factor flag must be enabled for either two step or two factor.");
            });

            RuleFor(x => x)
                .Must(HaveTwoStepOrTwoFactor)
                .WithMessage("Either two step or two factor is required.");
        }

        private bool HaveTwoStepOrTwoFactor(SetMultiFactorCommand command)
        {
            return !command.TwoStepEnabled || !command.TwoFactorEnabled;
        }
    }
}