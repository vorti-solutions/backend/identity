﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Set2FA.Commands
{
    public class SetMultiFactorCommand : IRequest<BaseResponse>
    {
        public bool MultifactorEnabled { get; set; }
        public bool TwoStepEnabled { get; set; }
        public bool TwoFactorEnabled { get; set; }
    }
}