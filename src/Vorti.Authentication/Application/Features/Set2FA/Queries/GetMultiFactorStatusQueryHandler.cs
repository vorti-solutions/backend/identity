﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using Vorti.Authentication.Application.Contracts.Utility;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Application.Features.Set2FA.Queries
{
    public class GetMultiFactorStatusQueryHandler(
        IHttpContextAccessor httpContextAccessor,
        IUserIdentityService identityService,
        IResultResponse resultResponse) : IRequestHandler<GetMultiFactorStatusQuery, BaseResponse>
    {
        private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;
        private readonly IUserIdentityService _identityService = identityService;
        private readonly IResultResponse _resultResponse = resultResponse;

        public async Task<BaseResponse> Handle(GetMultiFactorStatusQuery request, CancellationToken cancellationToken)
        {
            var userClaimsResult = await _identityService.GetUserFromClaimsAsync(_httpContextAccessor.HttpContext.User);
            if (!userClaimsResult.IsSuccess)
                return userClaimsResult;

            var user = (userClaimsResult as ServiceResponse<UserResponse>).Payload.User;
            var multiFactor = user.TwoFactorEnabled || user.TwoFactorEnabled;

            return _resultResponse.Success(new UserMultiFactorResponse(multiFactor, user.TwoStepEnabled, user.TwoFactorEnabled));
        }
    }
}