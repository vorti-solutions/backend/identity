﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.Set2FA.Queries
{
    public class GetMultiFactorStatusQuery : IRequest<BaseResponse>
    {
    }
}