﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ChangeEmail
{
    public class CompleteChangeEmailCommand : IRequest<BaseResponse>
    {
        public string NewEmail { get; set; }
        public string Otp { get; set; }
    }
}