﻿using MediatR;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Features.ChangeEmail
{
    public class InitiateChangeEmailCommand(string email) : IRequest<BaseResponse>
    {
        public string NewEmail { get; set; } = email;
    }
}