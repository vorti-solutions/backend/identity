﻿using FluentValidation;
using FluentValidation.Validators;

namespace Vorti.Authentication.Application.Features.ChangeEmail
{
    public class InitiateChangeEmailCommandValidator : AbstractValidator<InitiateChangeEmailCommand>
    {
        public InitiateChangeEmailCommandValidator()
        {
            RuleFor(x => x.NewEmail)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);
        }
    }
}