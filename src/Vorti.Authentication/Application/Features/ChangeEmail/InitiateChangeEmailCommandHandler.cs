﻿using MediatR;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using Vorti.Authentication.Domain.Common.Enumerations;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Application.Features.ChangeEmail
{
    public class InitiateChangeEmailCommandHandler(
        IHttpContextAccessor httpContextAccessor,
        IUserIdentityService identityService) : IRequestHandler<InitiateChangeEmailCommand, BaseResponse>
    {
        private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;
        private readonly IUserIdentityService _identityService = identityService;

        public async Task<BaseResponse> Handle(InitiateChangeEmailCommand request, CancellationToken cancellationToken)
        {
            var userClaimsResult = await _identityService.GetUserFromClaimsAsync(_httpContextAccessor.HttpContext.User);
            if (!userClaimsResult.IsSuccess)
                return userClaimsResult;

            var user = (userClaimsResult as ServiceResponse<UserResponse>).Payload.User;
            return await _identityService.SendOtpAsync(request.NewEmail, OtpPurpose.ChangeEmail, isEmailChange: true, userId: user.Id);
        }
    }
}