﻿using FluentValidation;
using FluentValidation.Validators;
using Vorti.Authentication.Domain.Common.Utilities;

namespace Vorti.Authentication.Application.Features.ChangeEmail
{
    public class CompleteChangeEmailCommandValidator : AbstractValidator<CompleteChangeEmailCommand>
    {
        public CompleteChangeEmailCommandValidator()
        {
            RuleFor(x => x.NewEmail)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(x => x.Otp)
                .NotEmpty()
            .Matches(Constants.AppRegex.DIGIT)
            .MaximumLength(6);
        }
    }
}