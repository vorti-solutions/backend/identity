﻿using Vorti.Authentication.Domain.Common.Utilities;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Contracts.Utility
{
    public interface IResultResponse
    {
        BaseResponse Failure(
            ResponseCodes responseCode,
            int statusCode = StatusCodes.Status400BadRequest,
            string responseDescription = default,
            IList<string> reasons = default);

        ServiceResponse<T> Success<T>(
            T payload = default,
            int statusCode = StatusCodes.Status200OK) where T : class;

        ServiceResponse Success(int statusCode = StatusCodes.Status200OK);
    }
}