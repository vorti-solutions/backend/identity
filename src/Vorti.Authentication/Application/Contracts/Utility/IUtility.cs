﻿using static Vorti.Authentication.Domain.Common.Utilities.DTOs;

namespace Vorti.Authentication.Application.Contracts.Utility
{
    public interface IUtility
    {
        string HashWithSalt(string rawText, string salt = default);
        string ComputeHmacSha512(string dataToBeHashed, string key = default);
        string ComputeSHA512(string data);
        
        TRequest DecryptRequest<TRequest>(RequestPayload request) where TRequest : class;
    }
}