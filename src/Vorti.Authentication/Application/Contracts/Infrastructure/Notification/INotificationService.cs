﻿using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Application.Contracts.Infrastructure.Notification
{
    public interface INotificationService
    {
        Task<string> PrepareTemplateAsync(string fileName, IDictionary<string, string> placeholders);
        Task<bool> SendSmtpEmail(EmailRequest request);
        bool IsMailSent();
    }
}