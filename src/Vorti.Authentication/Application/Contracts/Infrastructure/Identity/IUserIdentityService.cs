﻿using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using Vorti.Authentication.Domain.Common.Enumerations;
using Vorti.Authentication.Application.Features.ResetPassword;
using Vorti.Authentication.Application.Features.Login.Commands;
using System.Security.Claims;
using Vorti.Authentication.Domain.Entities;
using Vorti.Authentication.Application.Features.ChangeEmail;

namespace Vorti.Authentication.Application.Contracts.Infrastructure.Identity
{
    public interface IUserIdentityService
    {
        Task<BaseResponse> ActivateAsync(Guid userId);
        Task<BaseResponse> DeactivateAsync(Guid userId);
        Task<BaseResponse> AuthenticateAsync(Guid userId);
        Task<BaseResponse> GetUserByEmailAsync(string email);
        Task<BaseResponse> AuthenticateAsync(LoginCommand request);
        Task<BaseResponse> UpdateUserOnboardingStatus(string email);
        Task<BaseResponse> GetUserFromClaimsAsync(ClaimsPrincipal claimsPrincipal);
        Task<BaseResponse> GetUserIdFromClaimsAsync(ClaimsPrincipal claimsPrincipal);
        Task<BaseResponse> UpdatePhoneNumberAsync(ApplicationUser user, string phoneNumber);
        Task<BaseResponse> ChangeEmailAsync(CompleteChangeEmailCommand request, ApplicationUser user);
        Task<BaseResponse> ChangePasswordAsync(ApplicationUser user, string currentPassword, string newPassword);
        Task<BaseResponse> SetMultifactorEnabledAsync(string email, bool isTwoStep = false, bool isTwoFactor = false);
        Task<BaseResponse> VerifyTwoFactorAsync(string email, string passCode);
        Task<BaseResponse> ResetPasswordAsync(CompleteResetPasswordCommand request);
        Task<BaseResponse> SendOtpAsync(string email, OtpPurpose otpPurpose, Role role = default, bool isEmailChange = false, Guid userId = default, string defaultPassword = default);
        Task<BaseResponse> SendOtpAsync(string email, int otpPurposeId);
        Task<BaseResponse> VerifyOtpAsync(string email, string otp);
        Task<BaseResponse> RegisterCustomerUserAsync(string email, string password, string phoneNumber, CancellationToken cancellation = default);
        Task<BaseResponse> OnboardUserAsync(string email, string phoneNumber, string role, CancellationToken cancellation = default);
    }
}