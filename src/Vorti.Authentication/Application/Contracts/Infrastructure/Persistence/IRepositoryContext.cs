﻿using MongoDB.Driver;

namespace Vorti.Authentication.Application.Contracts.Infrastructure.Persistence
{
    public interface IRepositoryContext
    {
        IMongoDatabase Repository { get; }
    }
}