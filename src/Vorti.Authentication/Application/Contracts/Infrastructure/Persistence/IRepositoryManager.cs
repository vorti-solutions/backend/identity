﻿using Vorti.Authentication.Domain.Entities;

namespace Vorti.Authentication.Application.Contracts.Infrastructure.Persistence
{
    public interface IRepositoryManager
    {
        IRepository<ApplicationUserRole> UserRole { get; }
        IRepository<ApplicationUser> User { get; }
    }
}