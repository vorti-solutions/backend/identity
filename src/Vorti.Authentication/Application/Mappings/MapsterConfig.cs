﻿using Mapster;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;
using Vorti.Authentication.Application.Features.Notifications.Email.Commands;

namespace Vorti.Authentication.Application.Mappings
{
    public class MapsterConfig
    {
        public static void Configure()
        {
            TypeAdapterConfig<EmailCommand, EmailRequest>.NewConfig();
        }
    }
}