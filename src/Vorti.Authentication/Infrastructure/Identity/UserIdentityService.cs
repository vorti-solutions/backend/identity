﻿using Google.Authenticator;
using GuardNet;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Vorti.Authentication.Application.Contracts.Infrastructure.Identity;
using Vorti.Authentication.Application.Contracts.Infrastructure.Notification;
using Vorti.Authentication.Application.Contracts.Infrastructure.Persistence;
using Vorti.Authentication.Application.Contracts.Utility;
using Vorti.Authentication.Application.Features.ChangeEmail;
using Vorti.Authentication.Application.Features.Login.Commands;
using Vorti.Authentication.Application.Features.ResetPassword;
using Vorti.Authentication.Domain.Common.Configs;
using Vorti.Authentication.Domain.Common.Enumerations;
using Vorti.Authentication.Domain.Common.Utilities;
using Vorti.Authentication.Domain.Entities;
using static Vorti.Authentication.Domain.Common.Utilities.Constants;
using static Vorti.Authentication.Domain.Common.Utilities.DTOs;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Infrastructure.Identity
{
    public class UserIdentityService(
        IUtility utility,
        IResultResponse result,
        IRepositoryManager repository,
        IOptions<AuthConfig> authConfig,
        IOptions<Google2FAConfig> twofaConfig,
        INotificationService notificationService,
        UserManager<ApplicationUser> userManager,
        RoleManager<ApplicationRole> roleManager,
        SignInManager<ApplicationUser> signInManager,
        IOptions<AccountActivationConfig> acctActivationConfig,
        ILogger<UserIdentityService> logger) : IUserIdentityService
    {
        private readonly IUtility _utility = utility;
        private readonly IResultResponse _result = result;
        private readonly IRepositoryManager _repository = repository;
        private readonly AuthConfig _authConfig = authConfig.Value;
        private readonly Google2FAConfig _2faConfig = twofaConfig.Value;
        private readonly INotificationService _notificationService = notificationService;
        private readonly UserManager<ApplicationUser> _userManager = userManager;
        private readonly RoleManager<ApplicationRole> _roleManager = roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager = signInManager;
        private readonly AccountActivationConfig _acctActivationConfig = acctActivationConfig.Value;
        private readonly ILogger<UserIdentityService> _logger = logger;
        private static readonly char[] separator = ['.', '@', '_', '-', ' '];

        public async Task<BaseResponse> ActivateAsync(Guid userId)
        {
            _logger.LogInformation("Activating user ===>> {userId}", userId.ToString());

            Guard.NotNullOrWhitespace(userId.ToString(), nameof(userId));
            var user = _userManager.Users.FirstOrDefault(x => x.Id == userId && !x.IsActive || !x.EmailConfirmed);
            if (user == null)
            {
                _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
            }

            user.ToggleActivateUser();
            if (!user.EmailConfirmed)
            {
                user.ToggleConfirmEmail();
                user.CompleteSignup();
            }

            user.UpdateLastModifiedDate();
            var updateUserResult = await _userManager.UpdateAsync(user);
            if (!updateUserResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.ActivateUserFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: updateUserResult.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success();
        }

        public async Task<BaseResponse> AuthenticateAsync(Guid userId)
        {
            _logger.LogInformation("Authenticating user ===>> {userId}", userId.ToString());

            Guard.NotNullOrWhitespace(userId.ToString(), nameof(userId));

            var user = _userManager.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
                _result.Failure(ResponseCodes.InvalidUserAccount);

            var canSignIn = await _signInManager.CanSignInAsync(user);
            if (!canSignIn || !user.IsActive)
            {
                return _result.Failure(ResponseCodes.UserLockedOrInactive, StatusCodes.Status401Unauthorized);
            }

            await _signInManager.SignInAsync(user, false);

            var tokenHandler = new JwtSecurityTokenHandler();
            var roles = await _userManager.GetRolesAsync(user);
            var token = await GenerateJwt(user, tokenHandler, roles);

            var access_token = tokenHandler.WriteToken(token);
            var lastLogin = user.LastLogin.HasValue ? DateTimeOffset.FromUnixTimeSeconds(user.LastLogin.Value).UtcDateTime : DateTime.UtcNow;

            user.RegisterLastLogin();
            var identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.SignInFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: identityResult.Errors.Select(x => x.Description).ToList());
            }

            var multfactor = user.TwoStepEnabled || user.TwoFactorEnabled;
            return _result.Success(new LoginResponse(multfactor, access_token, lastLogin));
        }

        public async Task<BaseResponse> AuthenticateAsync(LoginCommand request)
        {
            ApplicationUser user = null;
            SignInResult signInResult = null;

            if (request.IsEmail)
            {
                _logger.LogInformation("Authenticating user ===>> {Email}", request.Email);

                user = await _userManager.FindByEmailAsync(request.Email);
                if (user == null)
                {
                    return _result.Failure(ResponseCodes.InvalidLoginCredentials, StatusCodes.Status401Unauthorized);
                }
            }
            else
            {
                _logger.LogInformation("Authenticating user ===>> {PhoneNumber}", request.PhoneNumber);

                user = await _userManager.FindByNameAsync(request.PhoneNumber);
                if (user == null)
                {
                    return _result.Failure(ResponseCodes.InvalidLoginCredentials, StatusCodes.Status401Unauthorized);
                }
            }
            
            var canSignIn = await _signInManager.CanSignInAsync(user);
            if (!canSignIn || !user.IsActive)
            {
                return _result.Failure(ResponseCodes.UserLockedOrInactive, StatusCodes.Status401Unauthorized);
            }

            if (user.TwoStepEnabled || user.TwoFactorEnabled)
            {
                if (user.TwoStepEnabled)
                {
                    var otpResult = await SendOtpAsync(user.Email, OtpPurpose.Signin);
                    if (!otpResult.IsSuccess) return otpResult;

                    return _result.Success(new LoginResponse(multiFactor: true));
                }
                else
                {
                    if (user.TwoFactorEnabled)
                    {
                        return _result.Success(new LoginResponse(multiFactor: true));
                    }
                }
            }

            if (request.IsEmail)
            {
                signInResult = await _signInManager.PasswordSignInAsync(user, request.Password, false, true);
            }
            else
            {
                signInResult = await _signInManager.PasswordSignInAsync(request.PhoneNumber, request.Password, false, true);
            }

            if (!signInResult.Succeeded)
            {
                return _result.Failure(ResponseCodes.InvalidLoginCredentials, StatusCodes.Status401Unauthorized);
            }

            if (signInResult.IsLockedOut || signInResult.IsNotAllowed)
            {
                return _result.Failure(ResponseCodes.UserLocked, StatusCodes.Status401Unauthorized);
            }

            var accessFailedCountResult = await _userManager.ResetAccessFailedCountAsync(user);
            if (!accessFailedCountResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.UserResetFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: accessFailedCountResult.Errors.Select(x => x.Description).ToList());
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var roles = await _userManager.GetRolesAsync(user);
            var token = await GenerateJwt(user, tokenHandler, roles);

            var access_token = tokenHandler.WriteToken(token);
            var lastLogin = user.LastLogin.HasValue ? DateTimeOffset.FromUnixTimeSeconds(user.LastLogin.Value).UtcDateTime : DateTime.UtcNow;

            user.RegisterLastLogin();
            var identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.SignInFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: identityResult.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success(new LoginResponse(access_token: access_token, lastLogin: lastLogin));
        }

        public async Task<BaseResponse> ChangePasswordAsync(ApplicationUser user, string currentPassword, string newPassword)
        {
            _logger.LogInformation("Changing user password ===>> {Id}", user.Id.ToString());

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
            if (!changePasswordResult.Succeeded)
            {
                return _result.Failure(
                        ResponseCodes.ChangePasswordFailure,
                        StatusCodes.Status422UnprocessableEntity,
                        reasons: changePasswordResult.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success();
        }

        public async Task<BaseResponse> GetUserFromClaimsAsync(ClaimsPrincipal claimsPrincipal)
        {
            var user = await _userManager.GetUserAsync(claimsPrincipal);
            if (user == null)
            {
                return _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
            }

            return _result.Success(new UserResponse(user));
        }

        public async Task<BaseResponse> GetUserIdFromClaimsAsync(ClaimsPrincipal claimsPrincipal)
        {
            var userId = Guid.Parse(_userManager.GetUserId(claimsPrincipal));

            if (string.IsNullOrEmpty(userId.ToString()))
            {
                return _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
            }

            return await Task.FromResult(_result.Success(new UserIdResponse(userId)));
        }

        public async Task<BaseResponse> GetUserByEmailAsync(string email)
        {
            _logger.LogInformation("Getting user details ===>> {email}", email);
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
            }

            return _result.Success(new UserResponse(user));
        }

        public async Task<BaseResponse> DeactivateAsync(Guid userId)
        {
            _logger.LogInformation("Deactivating user ===>> {userId}", userId.ToString());
            Guard.NotNullOrWhitespace(userId.ToString(), nameof(userId));
            var user = _userManager.Users.FirstOrDefault(x => x.Id == userId && x.IsActive.Equals(true));
            if (user == null)
            {
                _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
            }

            user.ToggleDeactivateUser();
            user.UpdateLastModifiedDate();

            var updateUserResult = await _userManager.UpdateAsync(user);
            if (!updateUserResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.DeactivateUserFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: updateUserResult.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success();
        }

        public async Task<BaseResponse> RegisterCustomerUserAsync(string email, string password, string phoneNumber, CancellationToken cancellation = default)
        {
            _logger.LogInformation("Registering user ===>> {email}|{phoneNumber}", email, phoneNumber);

            Guard.NotNullOrWhitespace(email, nameof(email));
            Guard.NotNullOrWhitespace(password, nameof(password));

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                user = await _repository.User.SingleOrDefaultAsync(x => x.UserName.Equals(phoneNumber), cancellation);
                if (user != null)
                {
                    return _result.Failure(ResponseCodes.PhoneNumberAlreadyExist);
                }

                user = new ApplicationUser(phoneNumber, email, false, false);
                var userResult = await _userManager.CreateAsync(user, password);
                if (!userResult.Succeeded)
                {
                    return _result.Failure(
                        ResponseCodes.UserSignupFailure,
                        StatusCodes.Status422UnprocessableEntity,
                        reasons: userResult.Errors.Select(x => x.Description).ToList());
                }

                user.HashPassword(password);
                var userUpdateResult = await _userManager.UpdateAsync(user);
                if (!userUpdateResult.Succeeded)
                {
                    return _result.Failure(
                        ResponseCodes.UpdateRecordError,
                        StatusCodes.Status422UnprocessableEntity,
                        reasons: userUpdateResult.Errors.Select(x => x.Description).ToList());
                }
            }
            else
            {
                return _result.Failure(ResponseCodes.EmailTaken, StatusCodes.Status400BadRequest);
            }

            var appRole = await _roleManager.FindByNameAsync(Role.Customer.DisplayName);
            if (appRole == null)
            {
                return _result.Failure(ResponseCodes.InvalidRole);
            }

            var roleResult = await _userManager.AddToRoleAsync(user, Role.Customer.DisplayName);
            if (!roleResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.AssignUserRoleFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: roleResult.Errors.Select(x => x.Description).ToList());
            }

            user.InitiateSignup();
            user.UpdatePhoneNumber(phoneNumber);

            var userRoles = user.AddToRole(appRole);
            foreach (var userRole in userRoles)
                await _repository.UserRole.AddAsync(userRole, cancellation);

            var updateUserResult = await _userManager.UpdateAsync(user);
            if (!updateUserResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.UpdateRecordError,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: updateUserResult.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success(StatusCodes.Status201Created);
        }

        public async Task<BaseResponse> OnboardUserAsync(string email, string phoneNumber, string role, CancellationToken cancellation = default)
        {
            _logger.LogInformation("Onboarding user ===>> {email}|{phoneNumber}", email, phoneNumber);

            Guard.NotNullOrWhitespace(email, nameof(email));
            Guard.NotNullOrWhitespace(phoneNumber, nameof(phoneNumber));

            string defaultPassword = string.Empty;
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                user = await _repository.User.SingleOrDefaultAsync(x => x.UserName.Equals(phoneNumber), cancellation);
                if (user != null)
                {
                    return _result.Failure(ResponseCodes.PhoneNumberAlreadyExist);
                }

                var appRole = await _roleManager.FindByNameAsync(role);
                if (appRole == null)
                {
                    return _result.Failure(
                        ResponseCodes.InvalidRole,
                        StatusCodes.Status404NotFound);
                }

                defaultPassword = Helper.GenerateDefaultPassword(8);
                user = new ApplicationUser(phoneNumber, email, false, false);
                var userResult = await _userManager.CreateAsync(user, defaultPassword);
                if (!userResult.Succeeded)
                {
                    return _result.Failure(
                        ResponseCodes.UserSignupFailure,
                        StatusCodes.Status422UnprocessableEntity,
                        reasons: userResult.Errors.Select(x => x.Description).ToList());
                }

                var roleResult = await _userManager.AddToRoleAsync(user, role);
                if (!roleResult.Succeeded)
                {
                    return _result.Failure(
                        ResponseCodes.AssignUserRoleFailure,
                        StatusCodes.Status422UnprocessableEntity,
                        reasons: roleResult.Errors.Select(x => x.Description).ToList());
                }

                user.UpdatePhoneNumber(phoneNumber);
                user.UserOnboarding();

                var userRoles = user.AddToRole(appRole);
                foreach (var userRole in userRoles)
                    await _repository.UserRole.AddAsync(userRole, cancellation);

                var userUpdateResult = await _userManager.UpdateAsync(user);
                if (!userUpdateResult.Succeeded)
                {
                    return _result.Failure(
                        ResponseCodes.UpdateRecordError,
                        StatusCodes.Status422UnprocessableEntity,
                        reasons: userUpdateResult.Errors.Select(x => x.Description).ToList());
                }
            }
            else
            {
                return _result.Failure(ResponseCodes.EmailTaken);
            }

            return _result.Success(new UserResponse(user, defaultPassword), StatusCodes.Status201Created);
        }

        public async Task<BaseResponse> ChangeEmailAsync(CompleteChangeEmailCommand request, ApplicationUser user)
        {
            _logger.LogInformation("Changing user email address ===>> {NewEmail}", request.NewEmail);

            var otpResult = await VerifyOtpAsync(user.Email, request.Otp);
            if (!otpResult.IsSuccess)
            {
                return _result.Failure(
                        ResponseCodes.InvalidOtp,
                        reasons: otpResult.Error.Reasons);
            }

            user.UpdateEmail(request.NewEmail);
            user.UpdateLastModifiedDate();

            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded || result.Errors.Any())
            {
                return _result.Failure(
                    ResponseCodes.ChangeEmailFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: result.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success();
        }

        public async Task<BaseResponse> ResetPasswordAsync(CompleteResetPasswordCommand request)
        {
            _logger.LogInformation("Resetting user password ===>> {NewPassword}", request.NewPassword);

            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                return _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
            }

            var otpResult = await VerifyOtpAsync(user.Email, request.Otp);
            if (!otpResult.IsSuccess)
            {
                return _result.Failure(
                        ResponseCodes.InvalidOtp,
                        reasons: otpResult.Error.Reasons);
            }

            user.HashPassword(request.NewPassword);
            user.UpdateLastModifiedDate();

            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded || result.Errors.Any())
            {
                return _result.Failure(
                    ResponseCodes.UserResetFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: result.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success();
        }

        public async Task<BaseResponse> SendOtpAsync(string email, OtpPurpose otpPurpose, Role role = default, bool isEmailChange = false, Guid userId = default, string defaultPassword = default)
        {
            _logger.LogInformation("Sending otp to user ===>> {email}|{DisplayName}", email, otpPurpose.DisplayName);

            ApplicationUser user = null;
            Guard.NotNullOrWhitespace(email, nameof(email));
            Guard.NotNull(otpPurpose, nameof(otpPurpose));

            if (isEmailChange)
            {
                user = await _userManager.FindByIdAsync(userId.ToString());
                if (user == null)
                {
                    return _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
                }
            }
            else
            {
                user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                {
                    return _result.Failure(ResponseCodes.InvalidUserAccount, StatusCodes.Status401Unauthorized);
                }
            }

            user.GenerateOtp(_utility, otpPurpose);
            Dictionary<string, string> placeholders = null;

            string body;
            EmailRequest emailRequest = new();
            if (otpPurpose.DisplayName == OtpPurpose.Signup.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.OTP, user.Otp.GetOtp()},
                    {NotificationTemplate.OtpTemplate.Placeholders.USERNAME, user.Email.Split(separator, StringSplitOptions.RemoveEmptyEntries)[0]},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.SIGNUP_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.SIGNUP;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.Onboarding.DisplayName 
                && role != null && role.DisplayName == Role.Vendor.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_EMAIL, user.Email},
                    {NotificationTemplate.OtpTemplate.Placeholders.ACCOUNT_ACTIVATION_URL, _acctActivationConfig.VendorAccountActivationURL},
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_PASSWORD, defaultPassword},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.ONBOARDING_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.VENDOR_ONBOARDING;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.Onboarding.DisplayName 
                && role != null && role.DisplayName == Role.Driver.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_EMAIL, user.Email},
                    {NotificationTemplate.OtpTemplate.Placeholders.ACCOUNT_ACTIVATION_URL, _acctActivationConfig.DriverAccountActivationURL},
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_PASSWORD, defaultPassword},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.ONBOARDING_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.DRIVER_ONBOARDING;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.Onboarding.DisplayName 
                && role != null && role.DisplayName == Role.DeliveryAgent.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_EMAIL, user.Email},
                    {NotificationTemplate.OtpTemplate.Placeholders.ACCOUNT_ACTIVATION_URL, _acctActivationConfig.DeliveryAgencyAccountActivationURL},
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_PASSWORD, defaultPassword},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };
                 
                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.ONBOARDING_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.DELIVERY_AGENCY_ONBOARDING;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.Onboarding.DisplayName 
                && role != null && role.DisplayName == Role.Admin.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_EMAIL, user.Email},
                    {NotificationTemplate.OtpTemplate.Placeholders.ACCOUNT_ACTIVATION_URL, _acctActivationConfig.AdminAccountActivationURL},
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_PASSWORD, defaultPassword},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.ONBOARDING_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.ADMIN_ONBOARDING;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.Onboarding.DisplayName 
                && role != null && role.DisplayName == Role.SuperAdmin.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_EMAIL, user.Email},
                    {NotificationTemplate.OtpTemplate.Placeholders.ACCOUNT_ACTIVATION_URL, _acctActivationConfig.SuperAccountActivationURL},
                    {NotificationTemplate.OtpTemplate.Placeholders.ONBOARDING_PASSWORD, defaultPassword},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.ONBOARDING_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.SUPER_ADMIN_ONBOARDING;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.ResetPassword.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.OTP, user.Otp.GetOtp()},
                    {NotificationTemplate.OtpTemplate.Placeholders.USERNAME, user.Email.Split(separator, StringSplitOptions.RemoveEmptyEntries)[0]},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.RESET_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.RESET;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.ChangeEmail.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.OTP, user.Otp.GetOtp()},
                    {NotificationTemplate.OtpTemplate.Placeholders.USERNAME, user.Email.Split(separator, StringSplitOptions.RemoveEmptyEntries)[0]},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.CHANGE_EMAIL_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = email;
                emailRequest.Subject = NotificationTemplate.CHANGE_EMAIL;
                emailRequest.HtmlBody = body;
            }
            else if (otpPurpose.DisplayName == OtpPurpose.Signin.DisplayName)
            {
                placeholders = new Dictionary<string, string>{
                    {NotificationTemplate.OtpTemplate.Placeholders.OTP, user.Otp.GetOtp()},
                    {NotificationTemplate.OtpTemplate.Placeholders.USERNAME, user.Email.Split(separator, StringSplitOptions.RemoveEmptyEntries)[0]},
                    {NotificationTemplate.OtpTemplate.Placeholders.YEAR, DateTime.UtcNow.ToString("yyyy")}
                };

                body = await _notificationService.PrepareTemplateAsync(NotificationTemplate.OtpTemplate.SIGNIN_OTP_TEMPLATE, placeholders);
                emailRequest.ToAddress = user.Email;
                emailRequest.Subject = NotificationTemplate.SIGNIN;
                emailRequest.HtmlBody = body;
            }
            else
            {
                return _result.Failure(ResponseCodes.InvalidOtpTemplate);
            }

            var emailResult =  await _notificationService.SendSmtpEmail(emailRequest);
            if (!emailResult)
            {
                return _result.Failure(
                    ResponseCodes.SendOtpFailure,
                    StatusCodes.Status422UnprocessableEntity);
            }

            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded || result.Errors.Any())
            {
                return _result.Failure(
                    ResponseCodes.UpdateRecordError,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: result.Errors.Select(x => x.Description).ToList());
            }

            _logger.LogInformation("OTP_GENERATED =>" + user.Email.ToString());
            return _result.Success();
        }

        public async Task<BaseResponse> SendOtpAsync(string email, int otpPurposeId)
        {
            Guard.NotNullOrWhitespace(email, nameof(email));
            Guard.NotLessThan(otpPurposeId, 0, nameof(otpPurposeId));

            var otpPurpose = Enumeration.FromValue<OtpPurpose>(otpPurposeId);
            var otpResult = await SendOtpAsync(email, otpPurpose);
            if (!otpResult.IsSuccess)
                return otpResult;

            return _result.Success();
        }

        public async Task<BaseResponse> SetMultifactorEnabledAsync(string email, bool isTwoStep = false, bool isTwoFactor = false)
        {
            _logger.LogInformation("Setting user multifactor ===>> {email}", email);

            string qrCodeImageUrl = string.Empty;
            string setupCode = string.Empty;

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return _result.Failure(ResponseCodes.InvalidUserAccount);
            }

            user.SetTwoStepEnabled(isTwoStep);
            await _userManager.SetTwoFactorEnabledAsync(user, isTwoFactor);
            if (isTwoFactor)
            {
                TwoFactorAuthenticator tfa = new();
                var userUniqueKey = Guid.NewGuid().ToString().Replace("-", string.Empty);
                var setupInfo = tfa.GenerateSetupCode(_2faConfig.Issuer, user.Email, userUniqueKey, false, _2faConfig.QR_PPM);
                qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                setupCode = setupInfo.ManualEntryKey;
                user.SetTwoFactorCode(userUniqueKey);
            }
            else
            {
                user.SetTwoFactorCode();
            }
            
            user.UpdateLastModifiedDate();
            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded || result.Errors.Any())
            {
                return _result.Failure(
                    ResponseCodes.UpdateRecordError,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: result.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success(new UserTwoFactorResponse(qrCodeImageUrl, setupCode));
        }

        public async Task<BaseResponse> VerifyTwoFactorAsync(string email, string passCode)
        {
            _logger.LogInformation("Verifying user two factor pass code ===>> {email}|{passCode}", email, passCode);

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return _result.Failure(ResponseCodes.InvalidUserAccount);
            }

            TwoFactorAuthenticator tfa = new();
            var userUniqueKey = user.TwoFactorCode;
            var isValid = tfa.ValidateTwoFactorPIN(userUniqueKey, passCode);
            if (!isValid) return _result.Failure(ResponseCodes.Invalid2FACode);

            return _result.Success();
        }

        public async Task<BaseResponse> UpdatePhoneNumberAsync(ApplicationUser user, string phoneNumber)
        {
            _logger.LogInformation("Updating user phone number ===>> {Email}|{phoneNumber}", user.Email, phoneNumber);

            Guard.NotNullOrWhitespace(phoneNumber, nameof(phoneNumber));

            user.UpdatePhoneNumber(phoneNumber);
            user.UpdateLastModifiedDate();

            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded || result.Errors.Any())
            {
                return _result.Failure(
                    ResponseCodes.ChangePhoneNumberFailure,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: result.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success();
        }

        public async Task<BaseResponse> UpdateUserOnboardingStatus(string email)
        {
            _logger.LogInformation("Updating user onboarding status ===>> {email}", email);
            var userResult = await GetUserByEmailAsync(email);
            if (!userResult.IsSuccess)
            {
                return userResult;
            }

            var user = (userResult as ServiceResponse<UserResponse>)?.Payload.User;
            user.CompleteSignup();

            var updateUserResult = await _userManager.UpdateAsync(user);
            if (!updateUserResult.Succeeded)
            {
                return _result.Failure(
                    ResponseCodes.UpdateRecordError,
                    StatusCodes.Status422UnprocessableEntity,
                    reasons: updateUserResult.Errors.Select(x => x.Description).ToList());
            }

            return _result.Success();
        }

        public async Task<BaseResponse> VerifyOtpAsync(string email, string otp)
        {
            _logger.LogInformation("Verifying user otp ===>> {email}|{otp}", email, otp);

            Guard.NotNullOrWhitespace(email, nameof(email));
            Guard.NotNullOrWhitespace(otp, nameof(otp));

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return _result.Failure(ResponseCodes.InvalidUserAccount);
            }

            var otpResult = user.Otp.Verify(otp, _utility);
            if (!otpResult.Item1)
            {
                return _result.Failure(
                    ResponseCodes.InvalidOtp,
                    reasons: [otpResult.Item2]);
            }

            return _result.Success();
        }

        private async Task<SecurityToken> GenerateJwt(ApplicationUser user, JwtSecurityTokenHandler tokenHandler, IList<string> roles)
        {
            _logger.LogInformation("Generating user Json Web Token ===>> {Email}", user.Email);

            var key = Encoding.UTF8.GetBytes(_authConfig.Secret);
            var expiry = DateTime.UtcNow.AddMinutes(_authConfig.Expiration);
            var issuedAt = DateTime.UtcNow;

            var claims = new List<Claim>
            {
                new(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new(JwtRegisteredClaimNames.Email, user.Email),
                new(JwtRegisteredClaimNames.Exp, expiry.ToString()),
                new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new(JwtRegisteredClaimNames.Iss, _authConfig.ValidIssuer),
                new(JwtRegisteredClaimNames.Aud, _authConfig.ValidAudience),
                new(JwtRegisteredClaimNames.Iat, issuedAt.ToString()),
                new(JwtRegisteredClaimNames.Nbf, issuedAt.ToString()),
                new(ClaimTypes.MobilePhone, user.PhoneNumber)
            };

            foreach (var role in roles)
                claims.Add(new Claim(ClaimTypes.Role, role));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = _authConfig.ValidAudience,
                IssuedAt = issuedAt,
                Expires = expiry,
                Issuer = _authConfig.ValidIssuer,
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };

            return await Task.FromResult(tokenHandler.CreateToken(tokenDescriptor));
        }
    }
}