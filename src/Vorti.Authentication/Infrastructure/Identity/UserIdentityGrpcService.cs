using Grpc.Core;
using Vortify.Protobuf.V1.Identity;

namespace Vortify.Authentication.Infrastructure.Identity;

public class UserIdentityGrpcService : IdentityGrpcService.IdentityGrpcServiceBase
{
    private readonly ILogger<UserIdentityGrpcService> _logger;

    public UserIdentityGrpcService(ILogger<UserIdentityGrpcService> logger)
    {
        _logger = logger;
    }

    public override Task<HelloGrpcResponse> SayHello(
        HelloGrpcRequest request,
        ServerCallContext context
    )
    {
        _logger.LogInformation(
            "{class} -> {method} | hit",
            nameof(UserIdentityGrpcService),
            nameof(SayHello)
        );

        if (string.IsNullOrWhiteSpace(request.Name))
            return Task.FromResult(
                new HelloGrpcResponse { Greeting = "Hello! may I know your name? " }
            );

        return Task.FromResult(
            new HelloGrpcResponse { Greeting = $"Hello {request.Name}, nice to meet you!" }
        );
    }
}
