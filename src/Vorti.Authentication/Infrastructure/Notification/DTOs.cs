﻿using Mapster;
using Vorti.Authentication.Domain.Entities;

namespace Vorti.Authentication.Infrastructure.Notification
{
    public class DTOs
    {
        public record EmailRequest
        {
            public string ToAddress { get; set; }
            public string Subject { get; set; }
            public string FromAddress { get; set; }
            public string FromName { get; set; }
            public string HtmlBody { get; set; }
            public string StringBody { get; set; }
            [AdaptIgnore]
            public IList<Stream> Attachments { get; set; } = default;
        }

        public record UserIdResponse(Guid UserId);
        public record UserResponse(ApplicationUser User, string DefaultPassword = default);
        public record UserIsInRoleResponse(bool IsInRole);
        public record UserTwoFactorResponse(string QRCodeImageUrl, string SetupCode);
        public record UserMultiFactorResponse(bool MultiFactorEnabled, bool TwoStepEnabled, bool TwoFactorEnabled);
    }
}