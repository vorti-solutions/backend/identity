﻿using GuardNet;
using Microsoft.Extensions.Options;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using Vorti.Authentication.Application.Contracts.Infrastructure.Notification;
using Vorti.Authentication.Domain.Common.Configs;
using static Vorti.Authentication.Domain.Common.Utilities.Constants;
using static Vorti.Authentication.Infrastructure.Notification.DTOs;

namespace Vorti.Authentication.Infrastructure.Notification
{
    public class NotificationService(
        IOptions<EmailConfig> options, 
        IWebHostEnvironment hostEnvironment, 
        ILogger<NotificationService> logger) 
        : INotificationService
    {
        private bool _mailSent = false;

        private readonly EmailConfig _emailConfig = options.Value;
        private readonly IWebHostEnvironment _hostEnvironment = hostEnvironment;
        private readonly ILogger<NotificationService> _logger = logger;

        public bool IsMailSent() => _mailSent;

        public async Task<string> PrepareTemplateAsync(string fileName, IDictionary<string, string> placeholders)
        {
            Guard.NotNullOrWhitespace(fileName, nameof(fileName));
            Guard.NotAny(placeholders, nameof(placeholders));
            string path = Path.Combine(_hostEnvironment.WebRootPath, NotificationTemplate.FOLDER, fileName);
            var templateString = await File.ReadAllTextAsync(path, Encoding.UTF8);
            foreach (var item in placeholders)
                templateString = templateString.Replace(item.Key, item.Value);
            return templateString;
        }

        public async Task<bool> SendSmtpEmail(EmailRequest request)
        {
            Guard.NotNull(request, nameof(request));
            Guard.NotNullOrWhitespace(_emailConfig.FromName, nameof(_emailConfig.FromName));
            Guard.NotNullOrWhitespace(_emailConfig.Host, nameof(_emailConfig.Host));
            Guard.NotLessThanOrEqualTo(_emailConfig.Port, 0, nameof(_emailConfig.Port));
            Guard.NotNullOrWhitespace(request.ToAddress, nameof(request.ToAddress));

            var from = new MailAddress(request.FromAddress??_emailConfig.FromAddress, request.FromName??_emailConfig.FromName, Encoding.UTF8);
            var to = new MailAddress(request.ToAddress);
            using var message = new MailMessage(from, to);
            message.Body = request.HtmlBody ?? request.StringBody;
            message.IsBodyHtml = !string.IsNullOrWhiteSpace(request.HtmlBody);
            message.Subject = request.Subject;
            message.SubjectEncoding = Encoding.UTF8;

            if (request.Attachments != null && request.Attachments.Any())
            {
                foreach (var attachment in request.Attachments)
                {
                    message.Attachments.Add(new Attachment(attachment, MediaTypeNames.Application.Octet));
                }
            }

            try
            {
                using var client = new SmtpClient(_emailConfig.Host, _emailConfig.Port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(_emailConfig.UserName, _emailConfig.Password);
                client.EnableSsl = true;
                client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                await client.SendMailAsync(message);
            }
            catch (Exception)
            {
                return _mailSent;
            }

            return _mailSent;
        }

        private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            var token = Task.FromResult(e.UserState).Result;

            if (e.Cancelled)
            {
                _logger.LogInformation("[{0}] Send Canceled.", token ?? "Email");
            }

            if (e.Error != null)
            {
                _logger.LogError("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                _mailSent = true;
            }
        }
    }
}