﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Vorti.Authentication.Application.Contracts.Infrastructure.Persistence;
using Vorti.Authentication.Domain.Common.Configs;

namespace Vorti.Authentication.Infrastructure.Persistence
{
    public class RepositoryContext : IRepositoryContext
    {
        private readonly MongoDbConfig _mongoDbConfig;
        private readonly IMongoDatabase _mongoDatabase;

        public RepositoryContext(IOptions<MongoDbConfig> options)
        {
            _mongoDbConfig = options.Value;

            var client = new MongoClient(_mongoDbConfig.ConnectionString);
            _mongoDatabase = client.GetDatabase(_mongoDbConfig.Database);
        }

        public IMongoDatabase Repository => _mongoDatabase;
    }
}