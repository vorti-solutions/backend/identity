﻿using MongoDB.Driver;
using Vorti.Authentication.Domain.Common.Enumerations;
using Vorti.Authentication.Domain.Entities;

namespace Vorti.Authentication.Infrastructure.Persistence
{
    public class RepositoryContextSeed
    {
        public static void SeedAppUser(IMongoCollection<ApplicationUser> user)
        {
            bool isExist = user.Find(p => true).Any();
            if (!isExist)
            {
                user.InsertMany(GetPreconfiguredUsers());
            }
        }

        public static void SeedAppRoles(IMongoCollection<ApplicationRole> roles)
        {
            bool isExist = roles.Find(p => true).Any();
            if (!isExist)
            {
                roles.InsertMany(GetPreconfiguredRoles());
            }
        }

        public static void SeedIdentityUserRole(IMongoCollection<ApplicationUserRole> userRole)
        {
            bool isExist = userRole.Find(p => true).Any();
            if (!isExist)
            {
                userRole.InsertMany(GetPreconfiguredUserRoles());
            }
        }

        private static List<ApplicationUser> GetPreconfiguredUsers()
        {
            var appUser = new List<ApplicationUser>();

            var superAdmin = new ApplicationUser(
            new Guid("45dfaf9b-a382-4ea1-bb45-10e581c01fdf"),
            "+2347065309576",
            "princekasiaric@gmail.com",
            true,
            true)
            {
                NormalizedEmail = "princekasiaric@gmail.com".ToUpper(),
                SecurityStamp = Guid.NewGuid().ToString()
            };
            superAdmin.AddRole(new Guid("c10f409b-e7a0-4dd8-b580-6d682dbbcd99"));
            superAdmin.HashPassword("Kasiaric@21");
            superAdmin.UpdatePhoneNumber("+2347065309576");
            superAdmin.CompleteSignup();
            appUser.Add(superAdmin);

            var vendor = new ApplicationUser(
            new Guid("04AADA9E-8156-4796-8F60-EF83937B6D12"),
            "+2349076555206",
            "princekasiaric@outlook.com",
            true,
            true)
            {
                NormalizedEmail = "princekasiaric@outlook.com".ToUpper(),
                SecurityStamp = Guid.NewGuid().ToString()
            };
            vendor.AddRole(new Guid("30d43ce6-b1af-43cc-b427-4c7616eecd3d"));
            vendor.HashPassword("Kasiaric@12");
            vendor.UpdatePhoneNumber("+2349076555206");
            vendor.CompleteSignup();
            appUser.Add(vendor);

            return appUser;
        }

        private static IEnumerable<ApplicationRole> GetPreconfiguredRoles()
        {
            return
            [
                new() {
                    Id = new Guid("c10f409b-e7a0-4dd8-b580-6d682dbbcd99"),
                    Name = Role.SuperAdmin.DisplayName,
                    NormalizedName = Role.SuperAdmin.DisplayName.ToUpper(),
                    Description = "Performs all administrative activities, and it's the highest elevated right.",
                    DateCreated = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                },
                new() {
                    Id = new Guid("de8676aa-7dc8-4b08-bfc4-833df0d2f9d0"),
                    Name = Role.Admin.DisplayName,
                    NormalizedName = Role.Admin.DisplayName.ToUpper(),
                    Description = "Performs all administrative activities.",
                    DateCreated = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                },
                new() {
                    Id = new Guid("30d43ce6-b1af-43cc-b427-4c7616eecd3d"),
                    Name = Role.Vendor.DisplayName,
                    NormalizedName = Role.Vendor.DisplayName.ToUpper(),
                    Description = "Performs all vendor activities.",
                    DateCreated = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                },
                new() {
                    Id = new Guid("4b5de1b2-92e8-4feb-91e8-f366cbcc6127"),
                    Name = Role.DeliveryAgent.DisplayName,
                    NormalizedName = Role.DeliveryAgent.DisplayName.ToUpper(),
                    Description = "Performs all delivery agent activities.",
                    DateCreated = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                },
                new() {
                    Id = new Guid("f620929d-b982-4f67-9985-1c8f03a57c62"),
                    Name = Role.Driver.DisplayName,
                    NormalizedName = Role.Driver.DisplayName.ToUpper(),
                    Description = "Performs all driver activities.",
                    DateCreated = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                },
                new() {
                    Id = new Guid("07aea4e0-8021-45f9-8def-8c5c9ad0e24d"),
                    Name = Role.Customer.DisplayName,
                    NormalizedName = Role.Customer.DisplayName.ToUpper(),
                    Description = "Performs all customer activities.",
                    DateCreated = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                }
            ];
        }

        private static List<ApplicationUserRole> GetPreconfiguredUserRoles()
        {
            var userRoles = new List<ApplicationUserRole>
            {
                new() {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("45dfaf9b-a382-4ea1-bb45-10e581c01fdf"),
                    RoleId = new Guid("c10f409b-e7a0-4dd8-b580-6d682dbbcd99")
                },
                new() {
                    Id = Guid.NewGuid(),
                    UserId = new Guid("04AADA9E-8156-4796-8F60-EF83937B6D12"),
                    RoleId = new Guid("30d43ce6-b1af-43cc-b427-4c7616eecd3d")
                }
            };

            return userRoles;
        }
    }
}