﻿using MongoDB.Driver;
using System.Linq.Expressions;
using Vorti.Authentication.Application.Contracts.Infrastructure.Persistence;

namespace Vorti.Authentication.Infrastructure.Persistence
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IRepositoryContext _context;
        private readonly IMongoCollection<T> _collection;

        public Repository(IRepositoryContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

            var name = typeof(T).Name.EndsWith("User", StringComparison.OrdinalIgnoreCase) ? "Users" : "UserRoles";
            _collection = _context.Repository.GetCollection<T>(name);
        }

        public async Task AddAsync(T entity, CancellationToken cancellationToken)
        {
            await _collection.InsertOneAsync(entity, cancellationToken: cancellationToken);
        }

        public async Task<bool> DeleteAsync(Expression<Func<T, bool>> predicate, CancellationToken cancellationToken = default)
        {
            FilterDefinition<T> filter = Builders<T>.Filter.Where(predicate);
            DeleteResult deleteResult = await _collection.DeleteOneAsync(filter, cancellationToken);

            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }

        public async Task<T> SingleOrDefaultAsync(Expression<Func<T, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return await _collection.Find(predicate).FirstOrDefaultAsync(cancellationToken);
        }
    }
}