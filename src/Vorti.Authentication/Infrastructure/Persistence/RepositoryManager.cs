﻿using Vorti.Authentication.Application.Contracts.Infrastructure.Persistence;
using Vorti.Authentication.Domain.Entities;

namespace Vorti.Authentication.Infrastructure.Persistence
{
    public class RepositoryManager(
        IRepository<ApplicationUserRole> userRole,
        IRepository<ApplicationUser> user) : IRepositoryManager
    {
        private readonly IRepository<ApplicationUserRole> _userRole = userRole;
        private readonly IRepository<ApplicationUser> _user = user;

        public IRepository<ApplicationUserRole> UserRole => _userRole;

        public IRepository<ApplicationUser> User => _user;
    }
}