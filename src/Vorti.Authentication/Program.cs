using AspNetCoreRateLimit;
using Vorti.Authentication.API.Extensions;
using Vorti.Authentication.API.Middlewares;
using Vorti.Authentication.Application.Contracts.Utility;
using Vortify.Authentication.Infrastructure.Identity;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddEnvironmentVariables();

// Add services to the container.
builder.Services.AddCors(x =>
{
    x.AddDefaultPolicy(x =>
    {
        x.AllowAnyOrigin();
        x.AllowAnyHeader();
        x.AllowAnyMethod();
    });
});

builder.Services.AddMemoryCache(); //For the purpose of rate limiting
builder.Services.AddHttpContextAccessor();

builder.Services.RegisterApiVersion();
builder.Services.RegisterRateLimiting();
builder.Services.RegisterFilters();
builder.Services.RegisterMediatR();
builder.Services.RegisterMapster();
builder.Services.RegisterServices();
builder.Services.RegisterValidators();
builder.Services.RegisterAppsettingConfigs(builder.Configuration);
builder.Services.RegisterSwaggerGen();
builder.Services.RegisterAuthentication();
builder.Services.RegisterAuthorization();
builder.Services.RegisterRepositoryContext();
builder.Services.RegisterGrpc();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//builder.Host.UseSerilog(SeriLogger.Configure);
var app = builder.Build();
using var scope = app.Services.CreateScope();
var logger = scope.ServiceProvider.GetRequiredService<ILogger<object>>();
var resultResponse = scope.ServiceProvider.GetRequiredService<IResultResponse>();

var env = app.Environment;

// Configure the HTTP request pipeline.
app.UseHttpsRedirection();

app.UseSwagger();
app.UseSwaggerUI();

app.UseIpRateLimiting();
app.UseRouting();
app.UseCors();
app.UseForwardedHeaders();
app.UseAuthentication();
app.UseAuthorization();
app.UseAppExceptionHandler(logger, resultResponse);
app.MapControllers();
app.MapGrpcService<UserIdentityGrpcService>();

if (env.IsDevelopment())
    app.MapGrpcReflectionService();

app.Run();
